﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.GameUnits;
using ZombieGame.Resources;
using ZombieGame.FieldRender;
using ZombieGame.ColonyData;
using ZombieGame.IteratorInfo;

namespace ZombieGame
{
    [Serializable()]
    public class GameMemento: ISerializable
    {
        public Colony colony { get; private set; }
        public int Time { get; private set; }
        public int DaysAlive { get; private set; }
        public List<Monster> monsters { get; private set; }

        public GameMemento(Colony colony, List<Monster> monsters, int DaysAlive, int Time)
        {
            this.colony = colony;
            this.monsters = monsters;
            this.Time = Time;
            this.DaysAlive = DaysAlive;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Colony", colony);
            info.AddValue("Monsters", monsters);
            info.AddValue("Time", Time);
            info.AddValue("DaysAlive", DaysAlive);
        }

        public GameMemento(SerializationInfo info, StreamingContext context)
        {
            colony = (Colony)info.GetValue("Colony", typeof(Colony));
            monsters = (List<Monster>)info.GetValue("Monsters", typeof(List<Monster>));

            Time = (int)info.GetValue("Time", typeof(int));
            DaysAlive = (int)info.GetValue("DaysAlive", typeof(int));
        }
    }

    public class GameHistory:Iteratable
    { 
        public LinkedList<GameMemento> History { get; private set; }

        public int Count { get { return History.Count; } }

        public GameMemento this[int index] { get { return History.ToList()[index]; } }

        public GameHistory()
        {
            History = new LinkedList<GameMemento>();
        }

        public void SaveToHistory(GameMemento memento)
        {
            while (History.Count >= 3)
                History.RemoveFirst();

            History.AddLast(memento);
        }

        public Iterator CreateIterator()
        {
            return new MementoIterator(this);
        }
    }
}

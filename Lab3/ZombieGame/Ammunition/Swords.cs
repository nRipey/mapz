﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.GameUnits;

namespace ZombieGame.Ammunition
{
    public abstract class Sword
    {
        public Sword(string type, string specialEffect, double minDamageInfluece, double maxDamageInfluence)
        {
            this.Type = type;
            this.SpecialEffect = specialEffect;
            this.MinDamageInfluece = minDamageInfluece;
            this.MaxDamageInfluece = maxDamageInfluence;
        }

        public Sword() { }
        public string Type { get; private set; }
        public string SpecialEffect { get; private set; }
        public double MinDamageInfluece { get; private set; }
        public double MaxDamageInfluece { get; private set; }

        protected double GenerateDamage(double unitAttack, double min, double max)
        {
            Random random = new Random();
            return (random.NextDouble() * (max - min) + min);             
        }

        protected double GeneratePercents()
        {
            Random random = new Random();
            return random.Next(0, 101);
        }

        public abstract double Hit(double attack, Unit unit);


    }

    class SimpleSword:Sword
    {
        public SimpleSword():base("Simple", "None", 0.3, 0.7) { }
        public override double Hit(double attack, Unit unit)
        {
            return attack + attack * GenerateDamage(attack, MinDamageInfluece, MaxDamageInfluece);
        }
    }

    class StrongSword:Sword
    {
        public StrongSword() : base("Strong", "3% to kill opponent at once", 0.5, 1) { }
        public override double Hit(double attack, Unit unit)
        {
            if (GeneratePercents() < 3)
                return unit.health;
            else
                return attack + attack * GenerateDamage(attack, MinDamageInfluece, MaxDamageInfluece);
        }
    }

    class MagicSword:Sword
    {
        public MagicSword() : base("Magic", "1% to turn zombie into survivor", 0.5, 0.8) { }
        public override double Hit(double attack, Unit unit)
        {
            if(GeneratePercents() == 0)
                throw new NotImplementedException();
            else
                return attack + attack * GenerateDamage(attack, MinDamageInfluece, MaxDamageInfluece);
        }
    }
}

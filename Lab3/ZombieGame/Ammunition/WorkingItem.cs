﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieGame.Ammunition
{
    class WorkingItem
    {
        public int TimesToBeUsed { get; private set; }

        public void ReduceTimes()
        {
            this.TimesToBeUsed--;

            if (TimesToBeUsed == 0)
                throw new NotImplementedException();
        }

        public void GatherSupply() 
        { 
            //TODO!
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieGame.Ammunition
{
    /*ABSTRACT FACTORY*/
    public abstract class AmmunitionFactory
    {
        public abstract Sword CreateSword();
        public abstract Armor CreateArmor();
    }

    class SimpleAmmunitionFactory: AmmunitionFactory
    {
        public override Sword CreateSword()
        {
            return new SimpleSword();
        }

        public override Armor CreateArmor()
        {
            return new SimpleArmor();
        }
    }

    class StrongAmmunitionFactory : AmmunitionFactory
    {
        public override Sword CreateSword()
        {
            return new StrongSword();
        }

        public override Armor CreateArmor()
        {
            return new StrongArmor();
        }
    }

    class MagicAmmunitionFactory : AmmunitionFactory
    {
        public override Sword CreateSword()
        {
            return new MagicSword();
        }

        public override Armor CreateArmor()
        {
            return new MagicArmor();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;

namespace ZombieGame.MenuHandler
{
    public abstract class Command
    {
        protected MenuCommandReceiver commandReceiver;

        public Command(MenuCommandReceiver commandReceiver)
        {
            this.commandReceiver = commandReceiver;
        }
        abstract public void Execute();
    }

    class SaveCommand : Command
    {
        public SaveCommand(MenuCommandReceiver commandReceiver) : base(commandReceiver)
        { }
        public override void  Execute()
        {
            commandReceiver.Save();
        }
    }

    class LoadCommand : Command
    {
        public LoadCommand(MenuCommandReceiver commandReceiver) : base(commandReceiver)
        {        }

        public override void Execute()
        {
            commandReceiver.Load();
        }
    }

    class NewColonyCommand : Command
    {
        public NewColonyCommand(MenuCommandReceiver commandReceiver) : base(commandReceiver)
        {        }
        public override void Execute()
        {
            commandReceiver.StartNewColony();
        }
    }

    class NoCommand : Command
    {
        public NoCommand():base(null) { }
        public override void Execute()
        {

        }
    }
    public class ButtonActionInvoker
    {
        Command command;

        public ButtonActionInvoker()
        {
            command = new NoCommand();
        }

        public void SetCommand(Command command)
        {
            this.command = command;
        }
        public void DoAction()
        {
            command.Execute();
        }
    }

    public class MenuCommandReceiver
    {
        public string FilePath = @"D:\Labs4\MAPZ\ZombieGame\Saves\Data.dat";
        public Stream stream { get; private set; }
        public BinaryFormatter bf { get; private set; }
        public void Save()
        {
            GameMemento memento = new GameMemento(GameForm.myColony, GameForm.monsters, GameForm.daysAlive, GameForm.Seconds);
            stream = File.Open(FilePath, FileMode.Create);
            bf = new BinaryFormatter();

            bf.Serialize(stream, memento);

            stream.Close();
        }

        public void Load()
        {
            try
            {
                stream = File.Open(FilePath, FileMode.Open);

                if (stream.Length == 0)
                {
                    stream.Close();
                    throw new Exception("No data to restore!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            bf = new BinaryFormatter();

            GameMemento memento = (GameMemento)bf.Deserialize(stream);
            stream.Close();

            foreach (Form form in Application.OpenForms)
                form.Hide();

            GameForm gameForm = new GameForm(memento);

            foreach (Form form in Application.OpenForms)
                gameForm.Closed += (s, args) => form.Close();

            gameForm.Show();
        }

        public void StartNewColony()
        {
            foreach (Form form in Application.OpenForms)
                form.Hide();

            NewColonyMenu newColonyMenu = new NewColonyMenu();

            foreach (Form form in Application.OpenForms)
                newColonyMenu.Closed += (s, args) => form.Close();

            newColonyMenu.Show();
        }
    }
}

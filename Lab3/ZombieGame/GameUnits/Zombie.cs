﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.GameUnits;
using ZombieGame.ColonyData;
using System.Runtime.Serialization;

namespace ZombieGame.Zombies
{
    [Serializable()]
    class Zombie : Monster, ICloneable, ISerializable
    {
        public Zombie(double health, double attack, string type = "Custom Zombie") : base(health, attack, type) { }

        static public Zombie CloneFromCollection(UnitTypes type)
        {
            return PrototypeRegistry.GetZombieConfigByType(type).MemberwiseClone() as Zombie;
        }

        public override void Attack(Colony colony)
        {
            Random random = new Random();
            int victimIndex = random.Next(0, colony.Survivors.Count);

            colony.Survivors[victimIndex].TakeDamage(this.attack);
        }

        public object Clone()
        {
            return this.MemberwiseClone() as Zombie;
        }
        public Zombie(SerializationInfo info, StreamingContext context):base(info, context)
        { }
    }
}

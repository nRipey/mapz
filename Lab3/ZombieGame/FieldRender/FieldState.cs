﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZombieGame.GameUnits;

namespace ZombieGame.FieldRender
{
    public abstract class FieldState
    {
        protected void AddButton(FlowLayoutPanel panel, Monster monster)
        {
            Button button = new Button();

            button.Width = 100;
            button.Height = 70;
            button.ForeColor = Color.Black;
            button.Font = new Font(button.Font.FontFamily, 9);
            button.Text = monster.Type;
            button.Tag = monster;

            panel.Controls.Add(button);
            ToolTip healthToolTip = new ToolTip();
            healthToolTip.SetToolTip(button, monster.health.ToString());
        }

        protected virtual void SetContextMenu(FlowLayoutPanel panel, ContextMenuStrip menuStrip)
        {
            foreach (Button button in panel.Controls)
            {
                button.ContextMenuStrip = menuStrip;
                button.Click += (sender, EventArgs) => ShowSurvivorActionMenu(sender, EventArgs);
            }
        }
        protected void ShowSurvivorActionMenu(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var points = button.PointToClient(Cursor.Position);
            button.ContextMenuStrip.Show(button, new System.Drawing.Point(points.X, points.Y));
        }

        public abstract void Render(GameForm gameForm);
    }
    class DayState : FieldState
    {
        public override void Render(GameForm gameForm)
        {
            gameForm.StatepictureBox.Image = Image.FromFile("D:/Labs4/MAPZ/ZombieGame/Pics/day.png");
            SetContextMenu(gameForm.SurviorLayoutPanel, gameForm.SurvivorActionContexMenu);            
        }
    }
    class NightState : FieldState
    {
        public override void Render(GameForm gameForm)
        {
            gameForm.StatepictureBox.Image = Image.FromFile("D:/Labs4/MAPZ/ZombieGame/Pics/night.png");

            gameForm.MonsterLayoutPanel.Controls.Clear();
            gameForm.MonsterLayoutPanel.Visible = true;

            gameForm.MonsterLayoutPanel.AutoScroll = false;
            gameForm.MonsterLayoutPanel.HorizontalScroll.Enabled = false;
            gameForm.MonsterLayoutPanel.HorizontalScroll.Visible = false;
            gameForm.MonsterLayoutPanel.HorizontalScroll.Maximum = 0;
            gameForm.MonsterLayoutPanel.AutoScroll = true;

            foreach (var monster in GameForm.monsters)
            {
                AddButton(gameForm.MonsterLayoutPanel, monster);
            }

            SetContextMenu(gameForm.SurviorLayoutPanel, gameForm.NightSurvivorMenuStrip);
        }
    }
}
﻿namespace ZombieGame
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WelcomeBox = new System.Windows.Forms.TextBox();
            this.FoodLabel = new System.Windows.Forms.Label();
            this.FoodBox = new System.Windows.Forms.TextBox();
            this.WoodBox = new System.Windows.Forms.TextBox();
            this.WoodLabel = new System.Windows.Forms.Label();
            this.StoneBox = new System.Windows.Forms.TextBox();
            this.StoneLabel = new System.Windows.Forms.Label();
            this.LogtextBox = new System.Windows.Forms.TextBox();
            this.LogLabel = new System.Windows.Forms.Label();
            this.SurvivorInfoBox = new System.Windows.Forms.TextBox();
            this.SurvivorLabel = new System.Windows.Forms.Label();
            this.MenuButton = new System.Windows.Forms.Button();
            this.SurviorLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SurvivorActionContexMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gatherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.foodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.woodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeAmmunitiionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magicalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TimetextBox = new System.Windows.Forms.TextBox();
            this.WalltextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.StatepictureBox = new System.Windows.Forms.PictureBox();
            this.MonsterLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.NightSurvivorMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.attackRandomlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DaystextBox = new System.Windows.Forms.TextBox();
            this.Dayslabel = new System.Windows.Forms.Label();
            this.HealthToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.TestButton = new System.Windows.Forms.Button();
            this.SurvivorActionContexMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatepictureBox)).BeginInit();
            this.NightSurvivorMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // WelcomeBox
            // 
            this.WelcomeBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.WelcomeBox.BackColor = System.Drawing.Color.White;
            this.WelcomeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WelcomeBox.Location = new System.Drawing.Point(250, 12);
            this.WelcomeBox.Multiline = true;
            this.WelcomeBox.Name = "WelcomeBox";
            this.WelcomeBox.ReadOnly = true;
            this.WelcomeBox.Size = new System.Drawing.Size(694, 40);
            this.WelcomeBox.TabIndex = 1;
            this.WelcomeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FoodLabel
            // 
            this.FoodLabel.AutoSize = true;
            this.FoodLabel.BackColor = System.Drawing.Color.White;
            this.FoodLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FoodLabel.Location = new System.Drawing.Point(1305, 90);
            this.FoodLabel.Name = "FoodLabel";
            this.FoodLabel.Size = new System.Drawing.Size(47, 18);
            this.FoodLabel.TabIndex = 2;
            this.FoodLabel.Text = "Food:";
            // 
            // FoodBox
            // 
            this.FoodBox.BackColor = System.Drawing.Color.White;
            this.FoodBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FoodBox.Location = new System.Drawing.Point(1364, 86);
            this.FoodBox.Name = "FoodBox";
            this.FoodBox.ReadOnly = true;
            this.FoodBox.Size = new System.Drawing.Size(83, 26);
            this.FoodBox.TabIndex = 3;
            this.FoodBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // WoodBox
            // 
            this.WoodBox.BackColor = System.Drawing.Color.White;
            this.WoodBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WoodBox.Location = new System.Drawing.Point(1364, 130);
            this.WoodBox.Name = "WoodBox";
            this.WoodBox.ReadOnly = true;
            this.WoodBox.Size = new System.Drawing.Size(83, 26);
            this.WoodBox.TabIndex = 5;
            this.WoodBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // WoodLabel
            // 
            this.WoodLabel.AutoSize = true;
            this.WoodLabel.BackColor = System.Drawing.Color.White;
            this.WoodLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WoodLabel.Location = new System.Drawing.Point(1305, 134);
            this.WoodLabel.Name = "WoodLabel";
            this.WoodLabel.Size = new System.Drawing.Size(53, 18);
            this.WoodLabel.TabIndex = 4;
            this.WoodLabel.Text = "Wood:";
            // 
            // StoneBox
            // 
            this.StoneBox.BackColor = System.Drawing.Color.White;
            this.StoneBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StoneBox.Location = new System.Drawing.Point(1364, 177);
            this.StoneBox.Name = "StoneBox";
            this.StoneBox.ReadOnly = true;
            this.StoneBox.Size = new System.Drawing.Size(83, 26);
            this.StoneBox.TabIndex = 7;
            this.StoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // StoneLabel
            // 
            this.StoneLabel.AutoSize = true;
            this.StoneLabel.BackColor = System.Drawing.Color.White;
            this.StoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StoneLabel.Location = new System.Drawing.Point(1305, 181);
            this.StoneLabel.Name = "StoneLabel";
            this.StoneLabel.Size = new System.Drawing.Size(51, 18);
            this.StoneLabel.TabIndex = 6;
            this.StoneLabel.Text = "Stone:";
            // 
            // LogtextBox
            // 
            this.LogtextBox.BackColor = System.Drawing.Color.White;
            this.LogtextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogtextBox.Location = new System.Drawing.Point(0, 638);
            this.LogtextBox.Multiline = true;
            this.LogtextBox.Name = "LogtextBox";
            this.LogtextBox.ReadOnly = true;
            this.LogtextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LogtextBox.Size = new System.Drawing.Size(1459, 185);
            this.LogtextBox.TabIndex = 8;
            // 
            // LogLabel
            // 
            this.LogLabel.AutoSize = true;
            this.LogLabel.BackColor = System.Drawing.Color.White;
            this.LogLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogLabel.Location = new System.Drawing.Point(-3, 617);
            this.LogLabel.Name = "LogLabel";
            this.LogLabel.Size = new System.Drawing.Size(37, 18);
            this.LogLabel.TabIndex = 9;
            this.LogLabel.Text = "Log:";
            // 
            // SurvivorInfoBox
            // 
            this.SurvivorInfoBox.BackColor = System.Drawing.Color.White;
            this.SurvivorInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SurvivorInfoBox.Location = new System.Drawing.Point(12, 35);
            this.SurvivorInfoBox.Multiline = true;
            this.SurvivorInfoBox.Name = "SurvivorInfoBox";
            this.SurvivorInfoBox.ReadOnly = true;
            this.SurvivorInfoBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SurvivorInfoBox.Size = new System.Drawing.Size(232, 565);
            this.SurvivorInfoBox.TabIndex = 10;
            // 
            // SurvivorLabel
            // 
            this.SurvivorLabel.AutoSize = true;
            this.SurvivorLabel.BackColor = System.Drawing.Color.White;
            this.SurvivorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SurvivorLabel.Location = new System.Drawing.Point(12, 14);
            this.SurvivorLabel.Name = "SurvivorLabel";
            this.SurvivorLabel.Size = new System.Drawing.Size(94, 18);
            this.SurvivorLabel.TabIndex = 11;
            this.SurvivorLabel.Text = "Survivor Info:";
            // 
            // MenuButton
            // 
            this.MenuButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MenuButton.Location = new System.Drawing.Point(1354, 6);
            this.MenuButton.Name = "MenuButton";
            this.MenuButton.Size = new System.Drawing.Size(93, 45);
            this.MenuButton.TabIndex = 12;
            this.MenuButton.Text = "Menu";
            this.MenuButton.UseVisualStyleBackColor = true;
            this.MenuButton.Click += new System.EventHandler(this.MenuButton_Click);
            // 
            // SurviorLayoutPanel
            // 
            this.SurviorLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.SurviorLayoutPanel.ForeColor = System.Drawing.Color.Transparent;
            this.SurviorLayoutPanel.Location = new System.Drawing.Point(275, 122);
            this.SurviorLayoutPanel.Name = "SurviorLayoutPanel";
            this.SurviorLayoutPanel.Size = new System.Drawing.Size(1015, 81);
            this.SurviorLayoutPanel.TabIndex = 13;
            // 
            // SurvivorActionContexMenu
            // 
            this.SurvivorActionContexMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gatherToolStripMenuItem,
            this.makeAmmunitiionToolStripMenuItem});
            this.SurvivorActionContexMenu.Name = "SurvivorActionContexMenu";
            this.SurvivorActionContexMenu.Size = new System.Drawing.Size(178, 48);
            // 
            // gatherToolStripMenuItem
            // 
            this.gatherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.foodToolStripMenuItem,
            this.woodToolStripMenuItem,
            this.stoneToolStripMenuItem});
            this.gatherToolStripMenuItem.Name = "gatherToolStripMenuItem";
            this.gatherToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.gatherToolStripMenuItem.Text = "Gather";
            // 
            // foodToolStripMenuItem
            // 
            this.foodToolStripMenuItem.Name = "foodToolStripMenuItem";
            this.foodToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.foodToolStripMenuItem.Text = "Food";
            this.foodToolStripMenuItem.Click += new System.EventHandler(this.foodToolStripMenuItem_Click);
            // 
            // woodToolStripMenuItem
            // 
            this.woodToolStripMenuItem.Name = "woodToolStripMenuItem";
            this.woodToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.woodToolStripMenuItem.Text = "Wood";
            // 
            // stoneToolStripMenuItem
            // 
            this.stoneToolStripMenuItem.Name = "stoneToolStripMenuItem";
            this.stoneToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.stoneToolStripMenuItem.Text = "Stone";
            // 
            // makeAmmunitiionToolStripMenuItem
            // 
            this.makeAmmunitiionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleToolStripMenuItem,
            this.strongToolStripMenuItem,
            this.magicalToolStripMenuItem});
            this.makeAmmunitiionToolStripMenuItem.Name = "makeAmmunitiionToolStripMenuItem";
            this.makeAmmunitiionToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.makeAmmunitiionToolStripMenuItem.Text = "Make Ammunitiion";
            // 
            // simpleToolStripMenuItem
            // 
            this.simpleToolStripMenuItem.Name = "simpleToolStripMenuItem";
            this.simpleToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.simpleToolStripMenuItem.Text = "Simple";
            // 
            // strongToolStripMenuItem
            // 
            this.strongToolStripMenuItem.Name = "strongToolStripMenuItem";
            this.strongToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.strongToolStripMenuItem.Text = "Strong";
            // 
            // magicalToolStripMenuItem
            // 
            this.magicalToolStripMenuItem.Name = "magicalToolStripMenuItem";
            this.magicalToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.magicalToolStripMenuItem.Text = "Magical";
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimeLabel.Location = new System.Drawing.Point(1142, 28);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(104, 20);
            this.TimeLabel.TabIndex = 14;
            this.TimeLabel.Text = "Time till night:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TimetextBox
            // 
            this.TimetextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimetextBox.Location = new System.Drawing.Point(1252, 22);
            this.TimetextBox.Name = "TimetextBox";
            this.TimetextBox.Size = new System.Drawing.Size(96, 26);
            this.TimetextBox.TabIndex = 15;
            // 
            // WalltextBox
            // 
            this.WalltextBox.BackColor = System.Drawing.Color.White;
            this.WalltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WalltextBox.Location = new System.Drawing.Point(1053, 25);
            this.WalltextBox.Name = "WalltextBox";
            this.WalltextBox.ReadOnly = true;
            this.WalltextBox.Size = new System.Drawing.Size(83, 26);
            this.WalltextBox.TabIndex = 17;
            this.WalltextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(961, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Wall State:";
            // 
            // StatepictureBox
            // 
            this.StatepictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatepictureBox.Location = new System.Drawing.Point(0, 0);
            this.StatepictureBox.Name = "StatepictureBox";
            this.StatepictureBox.Size = new System.Drawing.Size(1459, 823);
            this.StatepictureBox.TabIndex = 19;
            this.StatepictureBox.TabStop = false;
            // 
            // MonsterLayoutPanel
            // 
            this.MonsterLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.MonsterLayoutPanel.ForeColor = System.Drawing.Color.Transparent;
            this.MonsterLayoutPanel.Location = new System.Drawing.Point(275, 429);
            this.MonsterLayoutPanel.Name = "MonsterLayoutPanel";
            this.MonsterLayoutPanel.Size = new System.Drawing.Size(1133, 171);
            this.MonsterLayoutPanel.TabIndex = 14;
            this.MonsterLayoutPanel.Visible = false;
            // 
            // NightSurvivorMenuStrip
            // 
            this.NightSurvivorMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attackRandomlyToolStripMenuItem});
            this.NightSurvivorMenuStrip.Name = "NightSurvivorMenuStrip";
            this.NightSurvivorMenuStrip.Size = new System.Drawing.Size(166, 26);
            // 
            // attackRandomlyToolStripMenuItem
            // 
            this.attackRandomlyToolStripMenuItem.Name = "attackRandomlyToolStripMenuItem";
            this.attackRandomlyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.attackRandomlyToolStripMenuItem.Text = "Attack Randomly";
            this.attackRandomlyToolStripMenuItem.Click += new System.EventHandler(this.attackRandomlyToolStripMenuItem_Click);
            // 
            // DaystextBox
            // 
            this.DaystextBox.BackColor = System.Drawing.Color.White;
            this.DaystextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DaystextBox.Location = new System.Drawing.Point(1124, 82);
            this.DaystextBox.Name = "DaystextBox";
            this.DaystextBox.ReadOnly = true;
            this.DaystextBox.Size = new System.Drawing.Size(73, 26);
            this.DaystextBox.TabIndex = 20;
            // 
            // Dayslabel
            // 
            this.Dayslabel.AutoSize = true;
            this.Dayslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Dayslabel.Location = new System.Drawing.Point(1203, 85);
            this.Dayslabel.Name = "Dayslabel";
            this.Dayslabel.Size = new System.Drawing.Size(87, 20);
            this.Dayslabel.TabIndex = 21;
            this.Dayslabel.Text = "day(s) alive";
            // 
            // HealthToolTip
            // 
            this.HealthToolTip.ShowAlways = true;
            // 
            // TestButton
            // 
            this.TestButton.Location = new System.Drawing.Point(785, 322);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(75, 23);
            this.TestButton.TabIndex = 22;
            this.TestButton.Text = "TestButton";
            this.TestButton.UseVisualStyleBackColor = true;
            this.TestButton.Click += new System.EventHandler(this.TestButton_Click);
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1459, 823);
            this.Controls.Add(this.TestButton);
            this.Controls.Add(this.Dayslabel);
            this.Controls.Add(this.DaystextBox);
            this.Controls.Add(this.MonsterLayoutPanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.WalltextBox);
            this.Controls.Add(this.TimetextBox);
            this.Controls.Add(this.TimeLabel);
            this.Controls.Add(this.SurviorLayoutPanel);
            this.Controls.Add(this.MenuButton);
            this.Controls.Add(this.SurvivorLabel);
            this.Controls.Add(this.SurvivorInfoBox);
            this.Controls.Add(this.LogLabel);
            this.Controls.Add(this.LogtextBox);
            this.Controls.Add(this.StoneBox);
            this.Controls.Add(this.StoneLabel);
            this.Controls.Add(this.WoodBox);
            this.Controls.Add(this.WoodLabel);
            this.Controls.Add(this.FoodBox);
            this.Controls.Add(this.FoodLabel);
            this.Controls.Add(this.WelcomeBox);
            this.Controls.Add(this.StatepictureBox);
            this.Name = "GameForm";
            this.Text = "Zombies!";
            this.SurvivorActionContexMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StatepictureBox)).EndInit();
            this.NightSurvivorMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox WelcomeBox;
        private System.Windows.Forms.Label FoodLabel;
        private System.Windows.Forms.TextBox FoodBox;
        private System.Windows.Forms.TextBox WoodBox;
        private System.Windows.Forms.Label WoodLabel;
        private System.Windows.Forms.TextBox StoneBox;
        private System.Windows.Forms.Label StoneLabel;
        private System.Windows.Forms.TextBox LogtextBox;
        private System.Windows.Forms.Label LogLabel;
        private System.Windows.Forms.TextBox SurvivorInfoBox;
        private System.Windows.Forms.Label SurvivorLabel;
        private System.Windows.Forms.Button MenuButton;
        private System.Windows.Forms.ToolStripMenuItem gatherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem foodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem woodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeAmmunitiionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strongToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem magicalToolStripMenuItem;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox TimetextBox;
        private System.Windows.Forms.TextBox WalltextBox;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.PictureBox StatepictureBox;
        public System.Windows.Forms.FlowLayoutPanel SurviorLayoutPanel;
        public System.Windows.Forms.ContextMenuStrip SurvivorActionContexMenu;
        public System.Windows.Forms.FlowLayoutPanel MonsterLayoutPanel;
        private System.Windows.Forms.ToolStripMenuItem attackRandomlyToolStripMenuItem;
        public System.Windows.Forms.ContextMenuStrip NightSurvivorMenuStrip;
        private System.Windows.Forms.TextBox DaystextBox;
        private System.Windows.Forms.Label Dayslabel;
        private System.Windows.Forms.ToolTip HealthToolTip;
        private System.Windows.Forms.Button TestButton;
    }
}


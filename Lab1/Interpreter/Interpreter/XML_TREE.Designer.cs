﻿namespace Interpreter
{
    partial class XML_TREES
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser_Tree = new System.Windows.Forms.WebBrowser();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // webBrowser_Tree
            // 
            this.webBrowser_Tree.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.webBrowser_Tree.Location = new System.Drawing.Point(41, 49);
            this.webBrowser_Tree.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.webBrowser_Tree.MinimumSize = new System.Drawing.Size(15, 16);
            this.webBrowser_Tree.Name = "webBrowser_Tree";
            this.webBrowser_Tree.Size = new System.Drawing.Size(584, 674);
            this.webBrowser_Tree.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(295, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "TREE";
            // 
            // XML_TREES
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(657, 734);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.webBrowser_Tree);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "XML_TREES";
            this.Text = "XML_TREES";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.WebBrowser webBrowser_Tree;
        private System.Windows.Forms.Label label1;
    }
}
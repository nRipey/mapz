﻿using System;
using System.Collections.Generic;


namespace Interpreter
{
    class VariableInfo
    {
        public TokenTypeToLexer Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public VariableInfo(TokenTypeToLexer Type, string Name, string Value)
        {
            this.Type = Type;
            this.Name = Name;
            this.Value = Value;
        }
    }

    class Variables
    {
       
        List<VariableInfo> m_ListVariables;
        public Variables()
        {
            m_ListVariables = new List<VariableInfo>();
        }

        public void Add(string Type, string Name, string Value="")
        {
            string _value = Value;
            TokenTypeToLexer type = (Type == "int") ? TokenTypeToLexer.INT : (Type == "bool") ? TokenTypeToLexer.BOOL : TokenTypeToLexer.STRING;
            if (_value == "")
            {
                if (type == TokenTypeToLexer.INT) _value = "0";
                else if (type == TokenTypeToLexer.BOOL) _value = "false";
            }
            m_ListVariables.Add(new VariableInfo(type, Name, _value));
        }

        public bool IsVariableDeclared(string name)
        {
            bool IsDeclared = false;
            m_ListVariables.ForEach(u => { if (u.Name == name) IsDeclared = true; });

            return IsDeclared;
        }

        public TokenTypeToLexer GetTypeOfVariable(Token temp)
        {
            string name = temp.value;
            TokenTypeToLexer type = TokenTypeToLexer.UNDEFINED;
            m_ListVariables.ForEach(u => { if (u.Name == name) type = u.Type;});
            return type;
        }

        public VariableInfo FindVariable(Token temp)
        {
            string name = temp.value;
            VariableInfo Variable = null;
            m_ListVariables.ForEach(u => { if (u.Name == name) Variable = u; });

            if (Variable == null) throw new Exception("Variable isn't declared!");

            return Variable;
        }
    }
}

﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;

namespace Interpreter.Interp
{
    class MyInterpreter
    {
        public XElement Program_Tree = new XElement("Program_Body");

        Form1 form;
        internal RichTextBox TextResult = new RichTextBox();

        public Data data;
        public class Data
        {
            public HtmlAgilityPack.HtmlDocument html { get; set; }
            public RichTextBox input { get; set; }
            public RichTextBox output { get; set; } = new RichTextBox();
            public XElement father { get; set; }

            public BodyExpression parser = new BodyExpression();

            public Data(RichTextBox outputResult, RichTextBox input, XElement father)
            {
                this.html = new HtmlAgilityPack.HtmlDocument();
                outputResult.Text = "";
                this.input = new RichTextBox();
                this.input.Text = input.Text;
                this.father = father;
            }
        }

        public MyInterpreter(Form1 form)
        {
            this.form = form;
            File.Delete(Form1.TreePath);

            data = new Data(form.rich_Output1, form.rich_Input, Program_Tree);
        }

        internal void ShowResult(object data_)
        {
            Data data = (Data)data_;

            try
            {

                Lexer lexer = new Lexer(data.input);
                lexer.LexerAnalize();
                Variables AllVariables = new Variables();

                data.parser = new BodyExpression(lexer.TokensList, new DataFromForm(AllVariables, data.html, data.output, data.father));

                data.parser.StartParsing();
                data.parser.BuildTree();

                form.rich_Output1.Text = data.output.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR!!!");
                return;
            }
        }

        public void Compile()
        {
            try
            {
                TextResult = new RichTextBox();

                data.output.Text = "";
                data.input.Text = form.rich_Input.Text;
                data.html = form.html_;

                ShowResult(data);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interpreter
{
    public partial class XML_TREES : Form
    {
        public XML_TREES(string Tree_xml)
        {
            InitializeComponent();
            webBrowser_Tree.Url = new System.Uri(String.Concat(Directory.GetCurrentDirectory().ToString(), "\\", Tree_xml));
        }

    }
}

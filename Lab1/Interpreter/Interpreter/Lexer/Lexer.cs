﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Interpreter
{
    enum TokenTypeToLexer
    {
        UNDEFINED = 0,
        OPERATOR, 
        VARIABLE, 
        SEPARATOR,
        STATEMENT,
        VALUE_TYPE,
        COMMAND,
        TAG, 
        ATTRIBUTE,
        INT,
        STRING,
        BOOL,
    }

    enum TokenType
    {
        //KEYWORDS
        WHILE,
        IF,
        ELSE,
        PRINT,
        FIND_BY_ID,
        FIND_BY_CLASS,
        FIND_BY_TAG,
        FIND_BY_ATTRIBUTE,
        COUNT_OF_TAG,
        COUNT_OF_CLASS,
        COUNT_OF_ATTRIBUTE,

        //PUNCTUATION
        SEMICOLON,
        COMMA,
        LEFT_PARENTHESIS,
        RIGHT_PARENTHESIS,
        LEFT_BRACKET,
        RIGHT_BRACKET,

        //ARITHMETIC OPERATIONS
        PLUS,
        MINUS,
        MULTIPLICATION,
        DIVISION,
        MOD,

        //LOGIC OPERATIONS
        EQUAL,
        NOT_EQUAL,
        GREATER,
        GREATER_EQUAL,
        LESS,
        LESS_EQUAL,

        //OPERATIONS
        ASSIGNMENT,

        //TYPES
        INT,
        BOOL,
        STRING,

        //BOOL
        TRUE,
        FALSE,

        VARIABLES,
        
        //HTML TAGS
        TAG_A,
        TAG_P,
        TAG_H1,
        TAG_H2,
        TAG_H3,
        TAG_H4,
        TAG_BUTTON,
        TAG_DIV,
        TAG_SPAN,
        TAG_INPUT,
        TAG_TEXTAREA,
        TAG_HTML,
        TAG_HEAD,
        TAG_BODY,
        TAG_IMG,
        TAG_TITLE,

        //ATTRIBUTES
        ATTRIBUTE_NAME,
        ATTRIBUTE_VALUE,
        ATTRIBUTE_ALT,
        ATTRIBUTE_CLASS,
        ATTRIBUTE_ID,
        ATTRIBUTE_READONLY,
        ATTRIBUTE_STYLE,
        ATTRIBUTE_HREF,
        ATTRIBUTE_TYPE,
    }
  
    class Token
    {
        #region LEXER DICTIONARY
        public static Dictionary<TokenType, string> Expressions = new Dictionary<TokenType, string>
        {
            //KEYWORDS
            {TokenType.WHILE, "while" },
            {TokenType.IF, "if" },
            {TokenType.ELSE, "else" },
            {TokenType.PRINT, "print" },
            {TokenType.FIND_BY_ID, "FindById" },
            {TokenType.FIND_BY_TAG, "FindByTag" },
            {TokenType.FIND_BY_CLASS, "FindByClass" },
            {TokenType.FIND_BY_ATTRIBUTE, "FindByAttribute" },
            {TokenType.COUNT_OF_TAG, "CountOfTag" },
            {TokenType.COUNT_OF_CLASS, "CountOfClass" },
            {TokenType.COUNT_OF_ATTRIBUTE, "CountOfAttribute" },

            //PUNCTUATION
            {TokenType.SEMICOLON, ";" },
            {TokenType.COMMA, "," },
            {TokenType.LEFT_PARENTHESIS, "(" },
            {TokenType.RIGHT_PARENTHESIS, ")" },
            {TokenType.LEFT_BRACKET, "{" },
            {TokenType.RIGHT_BRACKET, "}" },

            //LOGIC OPERATIONS
            {TokenType.EQUAL, "==" },
            {TokenType.NOT_EQUAL, "!=" },
            {TokenType.GREATER, ">" },
            {TokenType.GREATER_EQUAL, ">=" },
            {TokenType.LESS, "<" },
            {TokenType.LESS_EQUAL, "<=" },
            {TokenType.ASSIGNMENT, "=" },

            //ARITHMETIC OPERATIONS
            {TokenType.PLUS, "+" },
            {TokenType.MINUS, "-" },
            {TokenType.MULTIPLICATION, "*" },
            {TokenType.DIVISION, "/" },
            {TokenType.MOD, "%" },

            //TYPES
            {TokenType.BOOL, "bool" },
            {TokenType.STRING, "string" },
            {TokenType.INT, "int" },

            //BOOL
            {TokenType.TRUE, "true" },
            {TokenType.FALSE, "false" },

            //TAGS
            {TokenType.TAG_A, "a" },
            {TokenType.TAG_P, "p" },
            {TokenType.TAG_H1, "h1" },
            {TokenType.TAG_H2, "h2" },
            {TokenType.TAG_H3, "h3" },
            {TokenType.TAG_H4, "h4" },
            {TokenType.TAG_HEAD, "head" },
            {TokenType.TAG_BODY, "body" },
            {TokenType.TAG_HTML, "html" },
            {TokenType.TAG_INPUT, "input" },
            {TokenType.TAG_TEXTAREA, "textarea" },
            {TokenType.TAG_BUTTON, "button" },
            {TokenType.TAG_DIV, "div" },
            {TokenType.TAG_SPAN, "span" },
            {TokenType.TAG_IMG, "img" },
            {TokenType.TAG_TITLE, "title" },

            //ATTRIBUTES
            {TokenType.ATTRIBUTE_NAME, "name" },
            {TokenType.ATTRIBUTE_VALUE, "value" },
            {TokenType.ATTRIBUTE_ALT, "alt" },
            {TokenType.ATTRIBUTE_CLASS, "class" },
            {TokenType.ATTRIBUTE_ID, "id" },
            {TokenType.ATTRIBUTE_READONLY, "readonly" },
            {TokenType.ATTRIBUTE_STYLE, "style" },
            {TokenType.ATTRIBUTE_HREF, "href" },
            {TokenType.ATTRIBUTE_TYPE, "type" },
        };
        #endregion
        public string value { get; set; }
        public TokenTypeToLexer type {get;}

        public Token(TokenTypeToLexer type, string value)
        {
            this.value = value;
            this.type = type;
        }
    }
    class Lexer
    {

        #region TYPES
        public static string[] AllTagValues = new string[] { Token.Expressions[TokenType.TAG_A], Token.Expressions[TokenType.TAG_P], Token.Expressions[TokenType.TAG_H1], Token.Expressions[TokenType.TAG_H2],
                                                             Token.Expressions[TokenType.TAG_H3], Token.Expressions[TokenType.TAG_H4], Token.Expressions[TokenType.TAG_BUTTON], Token.Expressions[TokenType.TAG_DIV],
                                                             Token.Expressions[TokenType.TAG_SPAN], Token.Expressions[TokenType.TAG_INPUT], Token.Expressions[TokenType.TAG_TEXTAREA], Token.Expressions[TokenType.TAG_BODY],
                                                             Token.Expressions[TokenType.TAG_HEAD], Token.Expressions[TokenType.TAG_HTML], Token.Expressions[TokenType.TAG_IMG], Token.Expressions[TokenType.TAG_TITLE] };

        public static string[] AllSeparatorValues = new string[] {Token.Expressions[TokenType.SEMICOLON], Token.Expressions[TokenType.COMMA], Token.Expressions[TokenType.LEFT_BRACKET], Token.Expressions[TokenType.RIGHT_BRACKET],
                                                                   Token.Expressions[TokenType.LEFT_PARENTHESIS], Token.Expressions[TokenType.RIGHT_PARENTHESIS], Token.Expressions[TokenType.LESS], Token.Expressions[TokenType.GREATER] };
    
        public static string[] AllStatementValues = new string[] { Token.Expressions[TokenType.WHILE], Token.Expressions[TokenType.IF], Token.Expressions[TokenType.ELSE] };
        
        public static string[] AllTypesValues = new string[] { Token.Expressions[TokenType.INT], Token.Expressions[TokenType.STRING], Token.Expressions[TokenType.BOOL] };

        public static string[] AllOperatorValues = new string[] { Token.Expressions[TokenType.PLUS], Token.Expressions[TokenType.MINUS], Token.Expressions[TokenType.MULTIPLICATION], Token.Expressions[TokenType.DIVISION], Token.Expressions[TokenType.EQUAL],
                                                                  Token.Expressions[TokenType.NOT_EQUAL], Token.Expressions[TokenType.ASSIGNMENT], Token.Expressions[TokenType.LESS], Token.Expressions[TokenType.LESS_EQUAL], Token.Expressions[TokenType.GREATER],
                                                                  Token.Expressions[TokenType.GREATER_EQUAL], Token.Expressions[TokenType.MOD] };

        public static string[] AllCommandValues = new string[] { Token.Expressions[TokenType.PRINT], Token.Expressions[TokenType.FIND_BY_ID], Token.Expressions[TokenType.FIND_BY_CLASS], Token.Expressions[TokenType.FIND_BY_TAG], 
                                                                 Token.Expressions[TokenType.FIND_BY_ATTRIBUTE], Token.Expressions[TokenType.COUNT_OF_ATTRIBUTE], Token.Expressions[TokenType.COUNT_OF_CLASS], Token.Expressions[TokenType.COUNT_OF_TAG]};

        public static string[] AllAttributesValues = new string[] { Token.Expressions[TokenType.ATTRIBUTE_NAME], Token.Expressions[TokenType.ATTRIBUTE_VALUE], Token.Expressions[TokenType.ATTRIBUTE_READONLY],
                                                                     Token.Expressions[TokenType.ATTRIBUTE_ALT], Token.Expressions[TokenType.ATTRIBUTE_CLASS], Token.Expressions[TokenType.ATTRIBUTE_ID], Token.Expressions[TokenType.ATTRIBUTE_TYPE],
                                                                     Token.Expressions[TokenType.ATTRIBUTE_HREF], Token.Expressions[TokenType.ATTRIBUTE_STYLE]};
        #endregion
        public List<Token> TokensList { get; } = new List<Token>();

        StringBuilder m_ProgramText = new StringBuilder("");
       
        public Lexer(RichTextBox _text)
        {
            for (int i = 0; i < _text.Lines.Length; ++i)
            {
                m_ProgramText.Append(_text.Lines[i] + " ");
            }
        }

        private List<StringBuilder> SplitStringFromOtherCodeParts()
        {
            m_ProgramText.Replace("\t", "");
            List<StringBuilder> PartsOfText = new List<StringBuilder>();

            int IndexOpenOld = 0;
            int IndexOpen = 0;
            int IndexClose = 0;

            while (IndexOpenOld < m_ProgramText.Length)
            {
                IndexOpen = m_ProgramText.ToString().IndexOf('"',IndexOpenOld);

                if (IndexOpen == -1)
                {
                    PartsOfText.Add(new StringBuilder( m_ProgramText.ToString().Substring(IndexOpenOld, m_ProgramText.Length - IndexOpenOld)));
                    break;
                }

                IndexClose = m_ProgramText.ToString().IndexOf('"',IndexOpen+1);

                if (IndexClose == -1) throw new Exception("Wrong Syntax");

                PartsOfText.Add(new StringBuilder(m_ProgramText.ToString().Substring(IndexOpenOld, IndexOpen - IndexOpenOld)));
                PartsOfText.Add(new StringBuilder(m_ProgramText.ToString().Substring(IndexOpen, IndexClose - IndexOpen+1)));

                IndexOpenOld = IndexClose+1;
            }

            return PartsOfText;
        }

        private List<string> Separate()
        {
            string[] Separate = { "+", "-", "*", "/", "{", "}", "(", ")", ";", ",", "==", "!=", ">=", "<=", "%" };

            List<StringBuilder> PartsOfText = SplitStringFromOtherCodeParts();

            foreach (var elements in PartsOfText)
            {
                if (elements[0] != '"')
                {
                    foreach (var val in Separate)
                    {
                        elements.Replace(val, " " + val + " ");
                    }
                }
            }

            foreach (var elements in PartsOfText)
            {
                if (elements[0] != '"')
                {
                    //FOR = >= <=
                    for (int i = 0; i < elements.Length; ++i)
                    {
                        if (elements[i] == '>' && elements[i + 1] != '=')
                        {
                            elements.Replace(">", " > ", i++, 1);
                        }
                        else if (elements[i] == '<' && elements[i + 1] != '=')
                        {
                            elements.Replace("<", " < ", i++, 1);
                        }

                        else if (elements[i] == '=' && (i + 1 == elements.Length || elements[i + 1] != '=')
                            && (i == 0 || (elements[i - 1] != '=' && elements[i - 1] != '!' 
                            && elements[i - 1] != '>' && elements[i - 1] != '<')))
                            elements.Replace("=", " = ", i++, 1);
                    }
                }
            }

            List<String[]> AllParts = new List<string[]>();


            foreach (var val in PartsOfText)
            {
                if (val[0] != '"')
                {
                    AllParts.Add(val.ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                    AllParts.Add(new string[] { val.ToString() });
            }

            List<string> list = new List<string>();

            foreach (var val in AllParts)
            {
                foreach (var str in val)
                {
                    list.Add(str);
                }
            }

            return list;
        }
        public void LexerAnalize()
        {
            List<String> list = Separate();
           

            for (int i = 0; i < list.Count; ++i)
            {
                if (IsOperator(list[i])) TokensList.Add(new Token(TokenTypeToLexer.OPERATOR, list[i]));
                else if (IsTAG(list[i])) TokensList.Add(new Token(TokenTypeToLexer.TAG, list[i]));
                else if (IsAttribute(list[i])) TokensList.Add(new Token(TokenTypeToLexer.ATTRIBUTE, list[i]));
                else if (IsSeparator(list[i])) TokensList.Add(new Token(TokenTypeToLexer.SEPARATOR, list[i]));
                else if (IsStatement(list[i])) TokensList.Add(new Token(TokenTypeToLexer.STATEMENT, list[i]));
                else if (IsValueType(list[i])) TokensList.Add(new Token(TokenTypeToLexer.VALUE_TYPE, list[i]));
                else if (IsCommand(list[i])) TokensList.Add(new Token(TokenTypeToLexer.COMMAND, list[i]));
                else if (IsInt(list[i])) TokensList.Add(new Token(TokenTypeToLexer.INT, list[i]));
            
                else if (IsString(list[i])) TokensList.Add(new Token(TokenTypeToLexer.STRING, list[i].Substring(1, list[i].Length - 2)));
                else if (IsBool(list[i])) TokensList.Add(new Token(TokenTypeToLexer.BOOL, list[i]));
                else if (IsVariable(list[i])) TokensList.Add(new Token(TokenTypeToLexer.VARIABLE, list[i]));

                else TokensList.Add(new Token(TokenTypeToLexer.UNDEFINED, list[i]));
            }
           
            Unary_Minus_Plus();

            TokensList.Add(new Token(TokenTypeToLexer.SEPARATOR, ";"));
        }

        private void Unary_Minus_Plus()
        {
            //- a => -a !!!
            for (int i = TokensList.Count - 1; i >= 0; --i)
            {
                if ((i == 0 || (i != 0 && TokensList[i - 1].type == TokenTypeToLexer.OPERATOR)) && TokensList[i].value == "-")
                {
                    TokensList[i + 1].value = (Int32.Parse(TokensList[i + 1].value) * -1).ToString();
                    TokensList.RemoveAt(i);
                }
            }
            //+ a => a !!!
            for (int i = TokensList.Count - 1; i >= 0; --i)
            {
                if ((i == 0 || (i != 0 && TokensList[i - 1].type == TokenTypeToLexer.OPERATOR)) && TokensList[i].value == "+")
                {
                    TokensList[i + 1].value = (Int32.Parse(TokensList[i + 1].value)).ToString();
                    TokensList.RemoveAt(i);
                }
            }

        }

        #region Define the Token type
        bool IsSeparator(string value) => AllSeparatorValues.Contains(value);

        bool IsStatement(string value) => AllStatementValues.Contains(value);

        bool IsValueType(string value) => AllTypesValues.Contains(value);

        bool IsOperator(string value) => AllOperatorValues.Contains(value);

        bool IsCommand(string value) => AllCommandValues.Contains(value);

        bool IsTAG(string value) => AllTagValues.Contains(value);

        bool IsAttribute(string value) => AllAttributesValues.Contains(value);

        bool IsBool(string value)
        {
            if (value == "true" || value == "false") return true;
            return false;
        }

        bool IsString(string value)
        {
            if (value[0] == '"' && value[value.Length - 1] == '"') return true;
            return false;
        }

        bool IsInt(string value)
        {
            var IsIntRegex = new Regex(@"^-?[0-9]+$");

            return IsIntRegex.IsMatch(value);
        }

        bool IsVariable(string value)
        {
            var IsVariableRegex = new Regex(@"^[A-Za-z_][A-Za-z_0-9]*$");

            return IsVariableRegex.IsMatch(value);
        }

       
        #endregion

    }
}

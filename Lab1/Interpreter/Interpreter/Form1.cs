﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Interpreter.Interp;

namespace Interpreter
{

    public partial class Form1 : Form
    {
        public const string TreePath = "Tree.xml";
        MyInterpreter interpreter;
        public HtmlAgilityPack.HtmlDocument html_ = new HtmlAgilityPack.HtmlDocument();

        public Form1()
        {
            InitializeComponent();

            rich_Input.AcceptsTab = true;
            MinimumSize = MaximumSize = Size;
            interpreter = new MyInterpreter(this);

        }

        #region BUTTONS
        private void btn_OpenHtmlFile1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                html_ = new HtmlAgilityPack.HtmlDocument();

                openFileDialog.Filter = "html files (*.html)|*.html";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var filePath = openFileDialog.FileName;
                    html_.Load(filePath);
                    rich_HTML1.Text = html_.Text;

                    webBrowser.Navigate(filePath);
                }
            }
        }
        private void menuItem1_Click(object sender, EventArgs e)
        {
            string Help = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, "HELP.txt");
            Process.Start(Help);
        }
        private void btn_Result_Click(object sender, EventArgs e)
        {
            interpreter = new MyInterpreter(this);
            interpreter.Compile();

            XmlDocument xdoc = new XmlDocument();

            xdoc.LoadXml(interpreter.Program_Tree.ToString());
            xdoc.Save(TreePath); 
        }

        private void bttn_ShowTrees_Click(object sender, EventArgs e)
        {
            XML_TREES form2 = new XML_TREES(TreePath);
            form2.Show();
        }
        #endregion
    }
}

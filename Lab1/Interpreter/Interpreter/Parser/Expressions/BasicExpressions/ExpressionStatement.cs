﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Interpreter
{
    /// <summary>
    /// Creating expression statement, such as var1 = var2 + 12 / 3 *33
    /// </summary>
    class ExpressionStatement : ExpressionAST
    {
        public ArithmeticTree tree;

        public override ExpressionAST ReturnChild() => this;
        public  ExpressionStatement(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {
            AllTokens = tokens;
            CountOfTokens = tokens.Count();    
        }

        public override bool CreateExpression(List<Token> tokens)
        {
            this.tokens = tokens;
            return true;
        }
        public Token FindResult()
        {
            tree = new ArithmeticTree(AllTokens, data);
            return tree.FindResult();
        }
    }
}

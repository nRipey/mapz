﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Interpreter
{
    class PrintExpression : ExpressionAST
    {
        public ExpressionStatement ExpressionToPrint;

      
        public PrintExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        { }
        public override ExpressionAST ReturnChild() => this;

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's print expression 
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.PRINT]) return false;
            #endregion
            return true;
        }

        public override bool CreateExpression(List<Token> tokens)
        {
            StartIndex = 0;
            this.tokens = tokens;

            #region Check if it's print expression 
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.PRINT]) return false;
            #endregion

            #region Check if print Statement is OK
            //print(aa); print(aa+12+"sas");
            if (tokens.Count - StartIndex < 5 || tokens[StartIndex + 1].value != Token.Expressions[TokenType.LEFT_PARENTHESIS]) throw new Exception("Wrong Print Statement !");

            AllTokens = GetTokensInExpressionUntilSeperator(ref StartIndex, tokens);

            if (AllTokens[AllTokens.Count - 2].value != Token.Expressions[TokenType.RIGHT_PARENTHESIS]) throw new Exception("Wrong Print Statement !");
            #endregion

            #region Printing 
            List<Token> Expression = AllTokens.GetRange(2, AllTokens.Count - 4);
            ExpressionToPrint = new ExpressionStatement(Expression, data);

            data.output.AppendText(ExpressionToPrint.FindResult().value + "\n");
            #endregion

            CountOfTokens = AllTokens.Count();

            return true;
        }
    }
}
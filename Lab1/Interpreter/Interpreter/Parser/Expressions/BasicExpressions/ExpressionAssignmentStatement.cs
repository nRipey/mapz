﻿using Interpreter.SyntaxTree;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Interpreter
{
    class ExpressionAssignmentStatement : ExpressionAST
    {
        public ExpressionStatement Expression;

        public override ExpressionAST ReturnChild() =>  this;
        public ExpressionAssignmentStatement(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {   }

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's Expression Assignment Statement
            if (!(tokens.Count > 1 && tokens[0].type == TokenTypeToLexer.VARIABLE && tokens[1].value == Operator_Assignment.Value)) 
                return false;
            #endregion

            return true;
        }

        public override bool CreateExpression(List<Token> tokens) 
        {
            #region initialization
            StartIndex = 0;
            this.tokens = tokens;
            CountOfTokens = tokens.Count();
            bool IsCorrect = false;
            List<Token> ListOfTokens = new List<Token>();
            #endregion

            #region Check if Expression Assignment Statement is OK
            if (tokens.Count - StartIndex < 4) throw new Exception("Wrong Expression Assignment Statement!");
            #endregion

            #region Create ExpressionAssignment statements
            while (StartIndex < tokens.Count)
            {
                ListOfTokens.Add(tokens[StartIndex]);
                if (tokens[StartIndex++].value == Token.Expressions[TokenType.SEMICOLON])
                {
                    IsCorrect = true;
                    break;
                }
            }
            if (!IsCorrect) throw new Exception("Wrong Expression Assignment Statement!");

            Expression = new ExpressionStatement(ListOfTokens.GetRange(0, ListOfTokens.Count - 1), data);
            Expression.FindResult();

            CountOfTokens = ListOfTokens.Count;

            #endregion

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Interpreter
{
    class InitializationExpression : ExpressionAST
    {
        public ExpressionStatement Expression;
        public InitializationExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        { }

        public override ExpressionAST ReturnChild() => this;

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's initialization expression
            if (tokens.Count < 1 || tokens[0].type != TokenTypeToLexer.VALUE_TYPE) return false;
            return true;
            #endregion
        }
        public override bool CreateExpression(List<Token> tokens)
        {
            const int MinCountOfTokens = 3;
            StartIndex = 0;

            #region Check if it's initialization expression
            if (tokens.Count < 1 || tokens[0].type != TokenTypeToLexer.VALUE_TYPE) return false;
            #endregion

            #region Check if Initialization Statement is OK
            AllTokens = GetTokensInExpressionUntilSeperator(ref StartIndex, tokens);
            CountOfTokens = AllTokens.Count();

            if (AllTokens.Count < MinCountOfTokens) throw new Exception("Wrong Initialization Statement!");
            #endregion


            //Initialize value by default;
            data.AllVariables.Add(AllTokens[0].value, AllTokens[1].value, "");

            //If initializiation without rvalue, for example : string s; int a;
            if (AllTokens.Count == MinCountOfTokens) return true;

            Expression = new ExpressionStatement(AllTokens.GetRange(1, AllTokens.Count - 2), data);

            Expression.FindResult();

            return true;
        }
    }
}
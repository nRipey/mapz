﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Interpreter
{
    class WhileExpression : ExpressionAST
    {
        List<Token> WhileConditional_ListOfTokens = new List<Token>();
        List<Token> WhileBody_ListOfTokens = new List<Token>();

        public ExpressionStatement WhileConditional;
        public BodyExpression WhileBody;
        public WhileExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {  }

        public override ExpressionAST ReturnChild() => this;

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's while expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.WHILE]) return false;
            #endregion

            return true;
        }
        public override bool CreateExpression(List<Token> tokens)
        {
            StartIndex = 0;
            this.tokens = tokens;

            #region Check if it's while expression
            if (tokens.Count < 1 ||  tokens[0].value != Token.Expressions[TokenType.WHILE]) return false;
            #endregion

            #region Check if While Statement is OK
            if (tokens.Count - this.StartIndex < 7 || tokens[this.StartIndex + 1].value != Token.Expressions[TokenType.LEFT_PARENTHESIS]) throw new Exception("Wrong While Statement!");
            #endregion

            #region WHILE CONDITIONAL

            WhileConditional_ListOfTokens = CreateConditional();

            if (WhileConditional_ListOfTokens.Count < 1 && tokens[this.StartIndex].value != Token.Expressions[TokenType.LEFT_BRACKET]) throw new Exception("Wrong While Statement!");

            WhileConditional = new ExpressionStatement( WhileConditional_ListOfTokens.GetRange(2, WhileConditional_ListOfTokens.Count - 3), data);
            #endregion

            #region WHILE BODY
            WhileBody_ListOfTokens = CreateBody();
            #endregion

            #region RUN WHILE
            Token result = WhileConditional.FindResult();
            WhileBody = new BodyExpression();
            int CountOfRepeat = 0;
            while (result.value == Token.Expressions[TokenType.TRUE] && result.type == TokenTypeToLexer.BOOL)
            {
                ++CountOfRepeat;
                WhileBody = new BodyExpression(WhileBody_ListOfTokens.GetRange(1, WhileBody_ListOfTokens.Count - 2), data);
                WhileBody.StartParsing();
                result = WhileConditional.FindResult();
            }
            #endregion

            CountOfTokens = WhileConditional_ListOfTokens.Count + WhileBody_ListOfTokens.Count;

           
                if (CountOfRepeat == 0)
                 {
                     WhileBody = new BodyExpression(WhileBody_ListOfTokens.GetRange(1, WhileBody_ListOfTokens.Count - 2), data);
                     WhileBody.data.output = new RichTextBox();
                     WhileBody.StartParsing();
                 }
                return true;
        }

        #region
        private List<Token> CreateBody()
        {
            List<Token> body  = new List<Token>();

            bool IsCorrect = false;
            int CountOfBrackets = 0;
            while (StartIndex < tokens.Count)
            {
                body.Add(tokens[StartIndex]);

                if (tokens[StartIndex].value == Token.Expressions[TokenType.LEFT_BRACKET])
                    ++CountOfBrackets;

                if (tokens[StartIndex++].value == Token.Expressions[TokenType.RIGHT_BRACKET])
                {
                    if (CountOfBrackets != 1)
                    {
                        --CountOfBrackets;
                        continue;
                    }
                    IsCorrect = true;
                    break;
                }
            }
            if (!IsCorrect) throw new Exception("Wrong While(body) Statement!");

            return body;
        }
        private List<Token> CreateConditional()
        {
            List<Token> body = new List<Token>();

            bool IsCorrect = false;
            int CountOfBrackets = 0;
            while (StartIndex < tokens.Count)
            {
                body.Add(tokens[StartIndex]);

                if (tokens[StartIndex].value == Token.Expressions[TokenType.LEFT_PARENTHESIS])
                    ++CountOfBrackets;

                if (tokens[StartIndex++].value == Token.Expressions[TokenType.RIGHT_PARENTHESIS])
                {
                    if (CountOfBrackets != 1)
                    {
                        --CountOfBrackets;
                        continue;
                    }
                    IsCorrect = true;
                    break;
                }
            }
            if (!IsCorrect) throw new Exception("Wrong While(Conditional) Statement!");

            return body;
        }
        #endregion
    }
}
    
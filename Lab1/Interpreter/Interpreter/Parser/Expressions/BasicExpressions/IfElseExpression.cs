﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Interpreter
{
    class IfExpression : ExpressionAST
    {
        #region fields
        List<Token> IfConditional_ListOfTokens = new List<Token>();
        List<Token> IfBody_ListOfTokens = new List<Token>();
        List<Token> ElseBody_ListOfTokens = new List<Token>();

        public ExpressionStatement IfConditionalExpression;
        public BodyExpression ElseBodyExpression;
        public BodyExpression IfBodyExpression;
        #endregion

        public IfExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        { this.data.father = new XElement("If"); }

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's If expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.IF]) return false;
            #endregion

            return true;
        }
        public override ExpressionAST ReturnChild() => this;
        public override bool CreateExpression(List<Token> tokens)
        {
            StartIndex = 0;

            #region Check if it's If expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.IF]) return false;

            this.tokens = tokens;

            if (tokens.Count - this.StartIndex < 7 || tokens[this.StartIndex + 1].value != Token.Expressions[TokenType.LEFT_PARENTHESIS]) throw new Exception("Wrong If Statement!");
            #endregion

            #region IF CONDITIONAL

            IfConditional_ListOfTokens = CreateIfConditional(); //GetAllTokensInExpression(ref this.StartIndex, tokens, Token.Expressions[TokenType.RIGHT_PARENTHESIS]);
            if (IfConditional_ListOfTokens.Count < 1 || tokens[1].value != Token.Expressions[TokenType.LEFT_PARENTHESIS]) throw new Exception("Wrong If Statement!");
            #endregion

            #region IF BODY
            List<Token> Expression = IfConditional_ListOfTokens.GetRange(2, IfConditional_ListOfTokens.Count - 3);
            IfConditionalExpression = new ExpressionStatement( Expression, data);

            IfBody_ListOfTokens = CreateIfBody();
            IfBodyExpression = new BodyExpression(IfBody_ListOfTokens.GetRange(1, IfBody_ListOfTokens.Count - 2), data);
            #endregion

            #region ELSE BODY

            ElseBodyExpression = null;

            if (this.StartIndex + 2 < tokens.Count && tokens[this.StartIndex].value == Token.Expressions[TokenType.ELSE])
            {
                ElseBody_ListOfTokens = CreateElseBody();
                ElseBodyExpression = new BodyExpression(ElseBody_ListOfTokens.GetRange(2, ElseBody_ListOfTokens.Count - 3), data);
            }
            #endregion

            #region RUN IF-ELSE

            IfConditionalExpression = new ExpressionStatement(IfConditional_ListOfTokens.GetRange(2, IfConditional_ListOfTokens.Count - 3), data);
            Token result = IfConditionalExpression.FindResult();

            if (result.value == Token.Expressions[TokenType.TRUE] && result.type == TokenTypeToLexer.BOOL)
            {
                IfBodyExpression.StartParsing();

                if (ElseBodyExpression != null)
                {
                    DataFromForm _data = new DataFromForm(ElseBodyExpression.data.AllVariables, ElseBodyExpression.data.html, new RichTextBox(), ElseBodyExpression.data.father);
                    ElseBodyExpression.data = _data;
                    ElseBodyExpression.StartParsing();
                }

            }
            else 
            {
                if (ElseBodyExpression != null) ElseBodyExpression.StartParsing();

                    DataFromForm _data = new DataFromForm(IfBodyExpression.data.AllVariables, IfBodyExpression.data.html, new RichTextBox(), IfBodyExpression.data.father);
                    IfBodyExpression.data = _data;
                    IfBodyExpression.StartParsing();
                
            }

            #endregion

          
            CountOfTokens = IfConditional_ListOfTokens.Count + IfBody_ListOfTokens.Count + ((ElseBodyExpression==null) ? 0 : ElseBody_ListOfTokens.Count);

            return true;
        }

        #region 
        private List<Token> CreateIfConditional()
        {
            bool IsCorrect = false;
            int CountOfBrackets = 0;

            List<Token> body = new List<Token>();

            while (StartIndex < tokens.Count)
            {
                body.Add(tokens[this.StartIndex]);

                if (tokens[StartIndex].value == Token.Expressions[TokenType.LEFT_PARENTHESIS])
                    ++CountOfBrackets;

                if (tokens[StartIndex++].value == Token.Expressions[TokenType.RIGHT_PARENTHESIS])
                {
                    if (CountOfBrackets != 1)
                    {
                        --CountOfBrackets;
                        continue;
                    }
                    IsCorrect = true;
                    break;
                }
            }
            if (!IsCorrect) throw new Exception("Wrong If(body) Statement!");

            return body;
        }

        private List<Token> CreateIfBody()
        {
            bool IsCorrect = false;
            int CountOfBrackets = 0;

            List<Token> body = new List<Token>();
            
            while (StartIndex < tokens.Count)
            {
                body.Add(tokens[this.StartIndex]);

                if (tokens[StartIndex].value == Token.Expressions[TokenType.LEFT_BRACKET])
                    ++CountOfBrackets;

                if (tokens[StartIndex++].value == Token.Expressions[TokenType.RIGHT_BRACKET])
                {
                    if (CountOfBrackets != 1)
                    {
                        --CountOfBrackets;
                        continue;
                    }
                    IsCorrect = true;
                    break;
                }
            }
            if (!IsCorrect) throw new Exception("Wrong If(body) Statement!");

            return body;
        }

        private List<Token> CreateElseBody()
        {
            bool IsCorrect = false;
            int CountOfBrackets = 0;

            List<Token> body = new List<Token>();

            if (StartIndex + 2 < tokens.Count && tokens[StartIndex].value == Token.Expressions[TokenType.ELSE])
            {
                if (tokens[StartIndex + 1].value != Token.Expressions[TokenType.LEFT_BRACKET]) throw new Exception("Wrong Else Statement!");
                IsCorrect = false;
                CountOfBrackets = 0;
                while (StartIndex < tokens.Count)
                {
                    body.Add(tokens[StartIndex]);

                    if (tokens[StartIndex].value == Token.Expressions[TokenType.LEFT_BRACKET])
                        ++CountOfBrackets;

                    if (tokens[StartIndex++].value == Token.Expressions[TokenType.RIGHT_BRACKET])
                    {
                        if (CountOfBrackets != 1)
                        {
                            --CountOfBrackets;
                            continue;
                        }
                        IsCorrect = true;
                        break;
                    }
                }
                if (!IsCorrect) throw new Exception("Wrong Else Statement!");    

            }

            return body;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Interpreter
{
    class FindByIdExpression : ExpressionAST
    {
        public Token Id = new Token(TokenTypeToLexer.STRING, "");
        public FindByIdExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {    }
        public override ExpressionAST ReturnChild() => this;

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's FindById expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.FIND_BY_ID]) return false;
            #endregion

            return true;
        }

        public override bool CreateExpression(List<Token> tokens)
        {
            this.tokens = tokens;
            const int MinCountOfTokens = 5;

            #region Check if it's FindById expression
            if (tokens.Count < 1 ||  tokens[0].value != Token.Expressions[TokenType.FIND_BY_ID]) return false;
            #endregion

            #region Check if FindById Statement is OK
            if (tokens.Count< MinCountOfTokens || tokens[1].value != "(") throw new Exception("Wrong FindById Statement!");

            AllTokens = GetTokensInExpressionUntilSeperator(ref StartIndex, tokens);


            if (AllTokens.Count != MinCountOfTokens || AllTokens[AllTokens.Count - 2].value != ")") throw new Exception("Wrong FindById Statement!");
            #endregion

            #region Find value 
            Token TokenId = AllTokens[2];
            Id.value = (TokenId.type == TokenTypeToLexer.VARIABLE) ? data.AllVariables.FindVariable(TokenId).Value : (TokenId.type == TokenTypeToLexer.STRING) ? TokenId.value : throw new Exception("Wrong FindById Statement!");

            if (data?.html?.Text == null || data?.html?.Text == "")
            {
                CountOfTokens = AllTokens.Count();
                data.output.AppendText($"Can't find by this id (\"{Id.value}\")\n");
                return true;
            }
            #endregion

            #region Find id by value
            var node = data.html.GetElementbyId(Id.value);

            if (node == null)
                data.output.AppendText($"Can't find by this id (\"{Id.value}\")\n");
            else
                data.output.AppendText(node.OuterHtml + "\n");

            // output.AppendText("\n");
            #endregion
            
            CountOfTokens = AllTokens.Count();

            return true;
        }
    }
}
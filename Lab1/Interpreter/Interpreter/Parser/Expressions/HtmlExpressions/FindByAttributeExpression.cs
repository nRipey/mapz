﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Interpreter
{
    class FindByAttributeExpression : ExpressionAST
    {
        public ExpressionStatement IndexStatement;
        public Token Attribute = new Token(TokenTypeToLexer.ATTRIBUTE, "");
        public FindByAttributeExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        { }
        public override ExpressionAST ReturnChild() => this;

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's FindByAttribute expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.FIND_BY_ATTRIBUTE]) return false;
            #endregion

            return true;
        }

        public override bool CreateExpression(List<Token> tokens)
        {
            this.tokens = tokens;
            const int MinCountOfTokens = 7;

            #region Check if it's FindByAttribute expression
            if (tokens.Count < 1 ||  tokens[0].value != Token.Expressions[TokenType.FIND_BY_ATTRIBUTE]) return false;
            #endregion

            #region Check if FindByAttribute Statement is OK
            if (tokens.Count  < MinCountOfTokens || tokens[1].value != "(") throw new Exception("Wrong FindByAttribute Statement!");


            AllTokens = GetTokensInExpressionUntilSeperator(ref StartIndex, tokens);


            if (AllTokens[AllTokens.Count - 2].value != ")" || AllTokens[3].value != ",") throw new Exception("Wrong FindByAttribute  Statement!");

            Token TokenAttribute = AllTokens[2];
            Attribute.value = (TokenAttribute.type == TokenTypeToLexer.ATTRIBUTE) ? TokenAttribute.value : throw new Exception("Wrong FindByAttribute Statement!");
            #endregion

            #region Find value and index 
            IndexStatement = new ExpressionStatement( AllTokens.GetRange(4, AllTokens.Count - 4 - 2), data);

            int Index = Int32.Parse(IndexStatement.FindResult().value);

            #endregion

            #region Find by attribute in all tags
            var nodes = new HtmlNodeCollection[Lexer.AllTagValues.Length];

            for (int i = 0; i < Lexer.AllTagValues.Length; ++i)
            {

                nodes[i] = data.html.DocumentNode.SelectNodes($"//{Lexer.AllTagValues[i]}[@{Attribute.value}]");
            }
            #endregion

            #region Find attribute-value by index
            if (nodes == null)
            {
                data.output.AppendText($"Can't find by this attribute ({Attribute})\n");
            }
            else
            {
                int CurrIndex = 0;
                foreach (var node in nodes)
                {
                    if (node != null)
                    {
                        foreach (var val in node)
                        {
                            if (CurrIndex++ == Index)
                            {
                                data.output.AppendText(val.OuterHtml + "\n");
                                data.output.AppendText("\n");
                                CountOfTokens = AllTokens.Count();

                                return true;
                            }
                        }
                    }
                }
            }

            data.output.AppendText($"Can't find by this Index ({Index})\n");
            #endregion

            CountOfTokens = AllTokens.Count();

            return true;
        }

    }
}

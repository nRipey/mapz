﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Interpreter
{
    class FindByClassExpression : ExpressionAST
    {
        public ExpressionStatement IndexStatement;
        public Token Class = new Token(TokenTypeToLexer.STRING, "");

        public FindByClassExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {  }

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's FindByClass expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.FIND_BY_CLASS]) return false;
            #endregion

            return true;
        }

        public override ExpressionAST ReturnChild() => this;
        public override bool CreateExpression(List<Token> tokens)
        {
            this.tokens = tokens;
            const int MinCountOfTokens = 7;
            
            #region Check if it's FindByClass expression
            if (tokens.Count < 1 ||  tokens[0].value != Token.Expressions[TokenType.FIND_BY_CLASS]) return false;
            #endregion

            #region Check if FindByClass Statement is OK
            //FindByClass("class1",index);

            if (tokens.Count < MinCountOfTokens || tokens[1].value != "(") throw new Exception("Wrong FindByClass Statement!");

            AllTokens = GetTokensInExpressionUntilSeperator(ref StartIndex, tokens);

            if (AllTokens[AllTokens.Count - 2].value != ")" || AllTokens[3].value != ",") throw new Exception("Wrong FindByClass Statement!");
            #endregion

            #region Find value and index 
            Token TokenClass = AllTokens[2];

            Class.value = (TokenClass.type == TokenTypeToLexer.STRING) ? TokenClass.value
                                : (TokenClass.type == TokenTypeToLexer.VARIABLE && data.AllVariables.FindVariable(TokenClass).Type == TokenTypeToLexer.STRING) ?
                                data.AllVariables.FindVariable(TokenClass).Value : throw new Exception("Wrong FindByClass Statement!");

            Class.value = "\"" + Class.value + "\"";


            IndexStatement = new ExpressionStatement(AllTokens.GetRange(4, AllTokens.Count - 4 - 2), data);

            int Index = Int32.Parse(IndexStatement.FindResult().value);
            #endregion

            #region Find by class in all tags
            var nodes = new HtmlNodeCollection[Lexer.AllTagValues.Length];

            for (int i = 0; i < Lexer.AllTagValues.Length; ++i)
            {
                nodes[i] = data.html.DocumentNode.SelectNodes($"//{Lexer.AllTagValues[i]}[@class = {Class.value}]");
            }
            #endregion

            #region Find class-value by index
            if (nodes == null)
            {
                data.output.AppendText($"Can't find by this index ({Index})\n");
            }
            else
            {
                int CurrIndex = 0;
                foreach (var node in nodes)
                {
                    if (node != null)
                    {
                        foreach (var val in node)
                        {
                            if (CurrIndex++ == Index)
                            {
                                data.output.AppendText(val.OuterHtml + "\n");
                                data.output.AppendText("\n");
                                CountOfTokens = AllTokens.Count;

                                return true;
                            }

                        }
                    }
                }
            }

            data.output.AppendText($"Can't find by this Index ({Index})\n");
            #endregion
            
            CountOfTokens = AllTokens.Count;

            return true;

        }

    }
}

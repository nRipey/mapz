﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Interpreter
{
    class FindByTagExpression : ExpressionAST
    {
        public ExpressionStatement IndexStatement;
        public Token Tag = new Token(TokenTypeToLexer.TAG, "");
        public FindByTagExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {  }

        public override ExpressionAST ReturnChild() => this;

        public override bool IfExpressionTrue(List<Token> tokens)
        {
            #region Check if it's FindByTag expression
            if (tokens.Count < 1 || tokens[0].value != Token.Expressions[TokenType.FIND_BY_TAG]) return false;
            #endregion

            return true;
        }

        public override bool CreateExpression(List<Token> tokens)
        {
            
            this.tokens = tokens;
            const int MinCountOfTokens = 7;

            #region Check if it's FindByTag expression
            if (tokens.Count < 1 ||  tokens[0].value != Token.Expressions[TokenType.FIND_BY_TAG]) return false;
            #endregion

            #region Check if FindByTag Statement is OK
            //FindByTag(p,2); FindByTag(a,1+2+3);

            if (tokens.Count  < MinCountOfTokens || tokens[1].value != "(") throw new Exception("Wrong FindByTag Statement!");

            AllTokens = GetTokensInExpressionUntilSeperator(ref StartIndex, tokens);

            if (AllTokens[AllTokens.Count - 2].value != ")" || AllTokens[3].value != ",") throw new Exception("Wrong FindByTag Statement!");
            #endregion

            #region Find value and index 
            Token TokenTag = AllTokens[2];
            Tag.value = (TokenTag.type == TokenTypeToLexer.TAG) ? TokenTag.value : throw new Exception("Wrong Tag Statement!");

            IndexStatement = new ExpressionStatement( AllTokens.GetRange(4, AllTokens.Count - 4 - 2),  data);

            int Index = Int32.Parse(IndexStatement.FindResult().value);
            #endregion

            #region Find tag-value by index
            var nodes = data.html.DocumentNode.Descendants(Tag.value);

            if (nodes == null)
            {
                data.output.AppendText($"Can't find by this tag ({Tag.value})\n");
            }
            else
            {
                if (Index >= nodes.Count() || Index < 0)
                    data.output.AppendText($"No element with this index ({Index})\n");
                else
                    data.output.AppendText(nodes.ElementAt(Index).OuterHtml + "\n");
            }
            //output.AppendText("\n");
            #endregion

            CountOfTokens = AllTokens.Count;
            
            return true;
        }
    }
}
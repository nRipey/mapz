﻿using System;
using System.Collections.Generic;


namespace Interpreter
{
    abstract class ExpressionAST 
    {
        #region fields
        public int CountOfTokens;
        public List<Token> AllTokens;
        protected int StartIndex;
        protected List<Token> tokens;
        public DataFromForm data;
        #endregion

        public ExpressionAST() { }

        public virtual bool IfExpressionTrue(List<Token> tokens) => true;
        public ExpressionAST(List<Token> tokens, DataFromForm data)
        {
            this.data = data;
            this.StartIndex = 0;
        }

        public abstract ExpressionAST ReturnChild();
        public int GetNext()
        {
            StartIndex = 0;
            return CountOfTokens;
        }

        public virtual bool CreateExpression(List<Token> tokens) => false;
        
        public static List<Token> GetTokensInExpressionUntilSeperator(ref int StartIndex, List<Token> tokens, string end = ";")
        {
            List<Token> ExpressionAssignment = new List<Token>();
            bool IsCorrect = false;

            while (StartIndex < tokens.Count)
            {
                ExpressionAssignment.Add(tokens[StartIndex]);
                if (tokens[StartIndex++].value == end)
                {
                    IsCorrect = true;
                    break;
                }
            }
            
            if (!IsCorrect)
                return null;
            else
                return ExpressionAssignment;
        }
    }
}

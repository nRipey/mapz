﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using static Interpreter.ArithmeticTree;

namespace Interpreter
{
    class DataFromForm
    {
        public Variables AllVariables;
        public HtmlAgilityPack.HtmlDocument html;
        public RichTextBox output;
        public XElement father;

        public DataFromForm(Variables AllVariables, HtmlAgilityPack.HtmlDocument html, RichTextBox output, XElement father)
        {
            this.AllVariables = AllVariables;
            this.html = html;
            this.output = output;
            this.father = (father != null) ? father : new XElement("Body");
        }
    }
    class BodyExpression :  ExpressionAST
    {
       
        #region
        public List<ExpressionAST> Children = new List<ExpressionAST>();
        #endregion

        public override ExpressionAST ReturnChild() => this;
        public BodyExpression()
        { }

        public BodyExpression(List<Token> tokens, DataFromForm data) : base(tokens, data)
        {
            this.tokens = tokens;    
        }
         public void StartParsing()
         {
            DataFromForm data_ = new DataFromForm(data.AllVariables, data.html, data.output, new XElement("_"));
            #region EXPRESSIONS
            List<ExpressionAST> expressions = new List<ExpressionAST>
            {
                new InitializationExpression(tokens, data_),
                new ExpressionAssignmentStatement(tokens, data_),
                new PrintExpression(tokens, data_),

                new IfExpression(tokens, data_),
                new WhileExpression(tokens, data_),

                new FindByAttributeExpression(tokens, data_),
                new FindByClassExpression(tokens, data_),
                new FindByIdExpression(tokens, data_),
                new FindByTagExpression(tokens, data_)
            };
            #endregion

                int index = 0;
                while (index + 1 < tokens.Count)
                {
                    bool IsRightCommand = false;


                    foreach (var val in expressions)
                    {
                        if (val.IfExpressionTrue(tokens.GetRange(index, tokens.Count - index)) == true)
                        {
                            dynamic temp = "";
                            if (val.ToString() == "Interpreter.InitializationExpression") temp = new InitializationExpression(tokens, data_);
                            else if (val.ToString() == "Interpreter.IfExpression") temp = new IfExpression(tokens, data_);
                            else if (val.ToString() == "Interpreter.ExpressionAssignmentStatement") temp = new ExpressionAssignmentStatement(tokens, data_);
                            else if (val.ToString() == "Interpreter.PrintExpression") temp = new PrintExpression(tokens, data_);
                            else if (val.ToString() == "Interpreter.WhileExpression") temp = new WhileExpression(tokens, data_);

                            else if (val.ToString() == "Interpreter.FindByAttributeExpression") temp = new FindByAttributeExpression(tokens, data_);
                            else if (val.ToString() == "Interpreter.FindByClassExpression") temp = new FindByClassExpression(tokens, data_);
                            else if (val.ToString() == "Interpreter.FindByIdExpression") temp = new FindByIdExpression(tokens, data_);
                            else if (val.ToString() == "Interpreter.FindByTagExpression") temp = new FindByTagExpression(tokens, data_);
                           
                            //if (temp == null) throw new Exception($"Wrong command !");
                            temp.CreateExpression(tokens.GetRange(index, tokens.Count - index));
                            Children.Add(temp.ReturnChild());
                            IsRightCommand = true;
                            index += temp.GetNext();
                            break;
                        }
                    }

                    if (index + 1 < tokens.Count && IsRightCommand == false)
                    {
                        throw new Exception($"No such command starts from {tokens[index].value} !");
                    }
                }
         }

        public void BuildTree()
        {
            CreateChild(Children, data.father);
        }

        void CreateChild(List<ExpressionAST> children, XElement father)
        {
            foreach (var child in children)
            {
                if (child.GetType().ToString() == "Interpreter.InitializationExpression")
                {
                    XElement IniatilizationNode = new XElement("Initialization");

                    Token type = ((InitializationExpression)child).Expression.tree.tree.LeftChild.CurrentValue;

                    IniatilizationNode.Add(new XAttribute("type", ((InitializationExpression)child).Expression.data.AllVariables.GetTypeOfVariable(type).ToString()));
                    AddExpression(IniatilizationNode, ((InitializationExpression)child).Expression.tree.tree);
                    father.Add(IniatilizationNode);
                }
                else if (child.GetType().ToString() == "Interpreter.PrintExpression")
                {
                    XElement IniatilizationNode = new XElement("Print"); 
                    AddExpression(IniatilizationNode, ((PrintExpression)child).ExpressionToPrint.tree.tree);
                    father.Add(IniatilizationNode);
                }
                else if (child.GetType().ToString() == "Interpreter.ExpressionAssignmentStatement")
                {
                    XElement IniatilizationNode = new XElement("Expression_Assignment");
                    AddExpression(IniatilizationNode, ((ExpressionAssignmentStatement)child).Expression.tree.tree);
                    father.Add(IniatilizationNode);
                }
                else if (child.GetType().ToString() == "Interpreter.IfExpression")
                {
                    if (((IfExpression)child).IfBodyExpression.Children.Count == 0 && ((IfExpression)child).ElseBodyExpression == null) continue;

                    XElement IniatilizationNode = new XElement("If_Expression"); 
                    XElement IfConditional = new XElement("If_Conditional");
                    XElement IfBody = new XElement("If_Body");
                    XElement ElseBody = ((IfExpression)child).ElseBodyExpression != null ? new XElement("Else_Body") : null;

                    AddExpression(IfConditional, ((IfExpression)child).IfConditionalExpression.tree.tree);
                    IniatilizationNode.Add(IfConditional);

                    if (((IfExpression)child).IfBodyExpression.Children.Count != 0)
                    {
                        CreateChild(((IfExpression)child).IfBodyExpression.Children, IfBody);
                        IniatilizationNode.Add(IfBody);
                    }

                    if (((IfExpression)child).ElseBodyExpression != null && ElseBody != null )
                    {
                        CreateChild(((IfExpression)child).ElseBodyExpression.Children, ElseBody);
             
                        IniatilizationNode.Add(ElseBody);
                    }              
         
                    father.Add(IniatilizationNode);
                }
                else if (child.GetType().ToString() == "Interpreter.WhileExpression")
                {
                    if ( ((WhileExpression)child).WhileBody.Children.Count == 0) continue;

                    XElement IniatilizationNode = new XElement("While_Expression"); 
                    XElement WhileConditional = new XElement("While_Conditional");
                    XElement WhileBody = new XElement("While_Body");
                
                    CreateChild(((WhileExpression)child).WhileBody.Children, WhileBody);

                    AddExpression(WhileConditional, ((WhileExpression)child).WhileConditional.tree.tree);

                    IniatilizationNode.Add(WhileConditional);
                    IniatilizationNode.Add(WhileBody);
                    
                    father.Add(IniatilizationNode);
                }
                else if (child.GetType().ToString() == "Interpreter.FindByAttributeExpression")
                {
                    //Coma
                    XElement FindByAttr = new XElement("Find_By_Attribute");
                    XElement ComaOperator = new XElement("OPERATOR");
                    ComaOperator.Add(",");

                    //Attr
                    Token temp = ((FindByAttributeExpression)child).Attribute;
                    XElement Attr = new XElement(temp.type.ToString());
                    Attr.Add(temp.value.ToString());
                    ComaOperator.Add(Attr);

                    //Index
                    AddExpression(ComaOperator, ((Interpreter.FindByAttributeExpression)child).IndexStatement.tree.tree);

                    FindByAttr.Add(ComaOperator);
                    father.Add(FindByAttr);
                }
                else if (child.GetType().ToString() == "Interpreter.FindByClassExpression")
                {
                    //Coma
                    XElement FindByAttr = new XElement("Find_By_Class");
                    XElement ComaOperator = new XElement("OPERATOR");
                    ComaOperator.Add(",");

                    //Attr
                    Token temp = ((FindByClassExpression)child).Class;
                    XElement Attr = new XElement(temp.type.ToString());
                    Attr.Add(temp.value.ToString());
                    ComaOperator.Add(Attr);

                    //Index
                    AddExpression(ComaOperator, ((Interpreter.FindByClassExpression)child).IndexStatement.tree.tree);

                    FindByAttr.Add(ComaOperator);
                    father.Add(FindByAttr);
                }
                else if (child.GetType().ToString() == "Interpreter.FindByTagExpression")
                {
                    //Coma
                    XElement FindByAttr = new XElement("Find_By_Tag");
                    XElement ComaOperator = new XElement("OPERATOR");
                    ComaOperator.Add(",");

                    //Attr
                    Token temp = ((FindByTagExpression)child).Tag;
                    XElement Attr = new XElement(temp.type.ToString());
                    Attr.Add(temp.value.ToString());
                    ComaOperator.Add(Attr);

                    //Index
                    AddExpression(ComaOperator, ((Interpreter.FindByTagExpression)child).IndexStatement.tree.tree);

                    FindByAttr.Add(ComaOperator);
                    father.Add(FindByAttr);
                }
                else if (child.GetType().ToString() == "Interpreter.FindByIdExpression")
                {

                    XElement FindById = new XElement("Find_By_Id");
                    Token temp = ((FindByIdExpression)child).Id;

                    temp.value = '"' + temp.value + '"';
                    
                    XElement Id = new XElement(temp.type.ToString());
                    Id.Add(temp.value.ToString());

                    FindById.Add(Id);
                    father.Add(FindById);
                }

            }
        }

 
        void AddExpression(XElement father, Tree current)
        {
            if (current == null) return;
           
            XElement curr = new XElement(current.CurrentValue.type.ToString());

            string value = current.CurrentValue.value;
            if (current.CurrentValue.type == TokenTypeToLexer.STRING) value = '"' + value + '"';
            curr.Add(value);

            AddExpression(curr, current.LeftChild);
            AddExpression(curr, current.RightChild);
            father.Add(curr);
        }
        
        public string GetByString(int index)
        {
            return data.output.Lines[index];
        }

        public int GetCountOfLines()
        {
            return data.output.Lines.Count();
        }
    }
}

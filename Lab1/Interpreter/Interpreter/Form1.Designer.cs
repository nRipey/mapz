﻿namespace Interpreter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rich_Input = new System.Windows.Forms.RichTextBox();
            this.rich_Output1 = new System.Windows.Forms.RichTextBox();
            this.btn_OpenHtmlFile1 = new System.Windows.Forms.Button();
            this.rich_HTML1 = new System.Windows.Forms.RichTextBox();
            this.btn_Result = new System.Windows.Forms.Button();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.bttn_ShowTrees = new System.Windows.Forms.Button();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // rich_Input
            // 
            this.rich_Input.BackColor = System.Drawing.Color.Black;
            this.rich_Input.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rich_Input.ForeColor = System.Drawing.Color.White;
            this.rich_Input.Location = new System.Drawing.Point(371, 10);
            this.rich_Input.Margin = new System.Windows.Forms.Padding(2);
            this.rich_Input.Name = "rich_Input";
            this.rich_Input.Size = new System.Drawing.Size(382, 244);
            this.rich_Input.TabIndex = 0;
            this.rich_Input.Text = "";
            // 
            // rich_Output1
            // 
            this.rich_Output1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rich_Output1.Location = new System.Drawing.Point(766, 10);
            this.rich_Output1.Margin = new System.Windows.Forms.Padding(2);
            this.rich_Output1.Name = "rich_Output1";
            this.rich_Output1.Size = new System.Drawing.Size(316, 244);
            this.rich_Output1.TabIndex = 1;
            this.rich_Output1.Text = "";
            // 
            // btn_OpenHtmlFile1
            // 
            this.btn_OpenHtmlFile1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_OpenHtmlFile1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_OpenHtmlFile1.Location = new System.Drawing.Point(9, 260);
            this.btn_OpenHtmlFile1.Margin = new System.Windows.Forms.Padding(0);
            this.btn_OpenHtmlFile1.Name = "btn_OpenHtmlFile1";
            this.btn_OpenHtmlFile1.Size = new System.Drawing.Size(346, 28);
            this.btn_OpenHtmlFile1.TabIndex = 2;
            this.btn_OpenHtmlFile1.Text = "Open Html File";
            this.btn_OpenHtmlFile1.UseVisualStyleBackColor = false;
            this.btn_OpenHtmlFile1.Click += new System.EventHandler(this.btn_OpenHtmlFile1_Click);
            // 
            // rich_HTML1
            // 
            this.rich_HTML1.Font = new System.Drawing.Font("Kristen ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rich_HTML1.Location = new System.Drawing.Point(9, 10);
            this.rich_HTML1.Margin = new System.Windows.Forms.Padding(2);
            this.rich_HTML1.Name = "rich_HTML1";
            this.rich_HTML1.Size = new System.Drawing.Size(347, 244);
            this.rich_HTML1.TabIndex = 3;
            this.rich_HTML1.Text = "";
            // 
            // btn_Result
            // 
            this.btn_Result.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Result.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Result.Location = new System.Drawing.Point(371, 260);
            this.btn_Result.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Result.Name = "btn_Result";
            this.btn_Result.Size = new System.Drawing.Size(381, 28);
            this.btn_Result.TabIndex = 10;
            this.btn_Result.Text = "Result";
            this.btn_Result.UseVisualStyleBackColor = false;
            this.btn_Result.Click += new System.EventHandler(this.btn_Result_Click);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "HELP";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // bttn_ShowTrees
            // 
            this.bttn_ShowTrees.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttn_ShowTrees.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bttn_ShowTrees.Location = new System.Drawing.Point(766, 258);
            this.bttn_ShowTrees.Margin = new System.Windows.Forms.Padding(2);
            this.bttn_ShowTrees.Name = "bttn_ShowTrees";
            this.bttn_ShowTrees.Size = new System.Drawing.Size(315, 28);
            this.bttn_ShowTrees.TabIndex = 11;
            this.bttn_ShowTrees.Text = "Show Trees";
            this.bttn_ShowTrees.UseVisualStyleBackColor = false;
            this.bttn_ShowTrees.Click += new System.EventHandler(this.bttn_ShowTrees_Click);
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(12, 303);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(1068, 380);
            this.webBrowser.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1092, 695);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.bttn_ShowTrees);
            this.Controls.Add(this.btn_Result);
            this.Controls.Add(this.rich_HTML1);
            this.Controls.Add(this.btn_OpenHtmlFile1);
            this.Controls.Add(this.rich_Output1);
            this.Controls.Add(this.rich_Input);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Interpreter";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_OpenHtmlFile1;
        private System.Windows.Forms.RichTextBox rich_HTML1;
        private System.Windows.Forms.Button btn_Result;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.Button bttn_ShowTrees;
        private System.Windows.Forms.WebBrowser webBrowser;
        public System.Windows.Forms.RichTextBox rich_Input;
        public System.Windows.Forms.RichTextBox rich_Output1;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.SyntaxTree
{
    class Operator_Plus : AbstractOperator
    {
        public static string Value = Token.Expressions[TokenType.PLUS];

        public override Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables)
        {
            #region Check whether it's Sum expression
            if (sign.value != Value) return null;
            #endregion

            base.GetResult(_operand1, _operand2, AllVariables);

            #region Run expression
            if (IsOperationWithNumbers())
            {
                return new Token(TokenTypeToLexer.INT, (Int32.Parse(operand1.value) + Int32.Parse(operand2.value)).ToString());
            }
            else
            {
                return new Token(TokenTypeToLexer.STRING, operand1.value + operand2.value);
            }
            #endregion
        }
    }
}

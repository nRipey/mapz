﻿using System;


namespace Interpreter.SyntaxTree
{
    class Operator_Multiplication : AbstractOperator
    {
        public static String Value = Token.Expressions[TokenType.MULTIPLICATION];

        public override Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables)
        {
            #region Check whether it's multiplication expression
            if (sign.value != Value) return null;
            #endregion
            
            base.GetResult(_operand1, _operand2, AllVariables);

            #region Run expression
            if (IsOperationWithNumbers())
            {
                return new Token(TokenTypeToLexer.INT, (Int32.Parse(operand1.value) * Int32.Parse(operand2.value)).ToString());
            }
            else
            {
                throw new Exception("Wrong Expression Assignment Statement!");
            }
            #endregion
        }
    }
}

﻿using System;

namespace Interpreter.SyntaxTree.Arithmetic
{
    class Operator_Mod : AbstractOperator
    {
        public static String Value = Token.Expressions[TokenType.MOD];

        public override  Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables)
        {
            #region Check whether it's mod expression
            if (sign.value != Value) return null;
            #endregion

            base.GetResult(_operand1, _operand2, AllVariables);

            #region Run expression
            if (IsOperationWithNumbers())
            {
                if (Int32.Parse(operand2.value) == 0) throw new Exception("Mod by zero!");

                return new Token(TokenTypeToLexer.INT, (Int32.Parse(operand1.value) % Int32.Parse(operand2.value)).ToString());
            }
            else
            {
                throw new Exception("Wrong Expression Assignment Statement!");
            }
            #endregion
        }
    }
}

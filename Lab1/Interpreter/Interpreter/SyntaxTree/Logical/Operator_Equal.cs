﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.SyntaxTree.Logical
{
    class Operator_Equal : AbstractOperator
    {
        public static String Value = Token.Expressions[TokenType.EQUAL];

        public override Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables)
        {
            if (sign.value != Value) return null;

            base.GetResult(_operand1, _operand2, AllVariables);

            if (operand1.type != operand2.type) throw new Exception("Different types of operands!");

            return new Token(TokenTypeToLexer.BOOL, (operand1.value == operand2.value).ToString().ToLower());
        }
    }
}

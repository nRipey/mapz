﻿using System;


namespace Interpreter.SyntaxTree.Logical
{
    class Operator_GreaterEqual : AbstractOperator
    {
        public static string Value = Token.Expressions[TokenType.GREATER_EQUAL];
        

        public override  Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables)
        {
            if (sign.value != Value) return null;

            base.GetResult(_operand1, _operand2, AllVariables);

            if (operand1.type != operand2.type) throw new Exception("Different types of operands!");

            if (IsOperationWithNumbers())
            {
                return new Token(TokenTypeToLexer.BOOL, (Int32.Parse(operand1.value) >= Int32.Parse(operand2.value)).ToString().ToLower());
            }
            else
            {
                return new Token(TokenTypeToLexer.BOOL, (String.Compare(operand1.value, operand2.value) >= 0).ToString().ToLower());
            }


        }
    }
}

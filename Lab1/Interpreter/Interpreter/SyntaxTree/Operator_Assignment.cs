﻿using System;


namespace Interpreter.SyntaxTree
{
    class Operator_Assignment : AbstractOperator
    {
        public static string Value = Token.Expressions[TokenType.ASSIGNMENT];

        public override Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables)
        {
            if (sign.value != Value) return null;

            base.GetResult(_operand1, _operand2, AllVariables);

            if (operand1.type != operand2.type)  throw new Exception("Different types of operands!");

            if (RealToken1.type != TokenTypeToLexer.VARIABLE) throw new Exception("Wrong Expression Assignment Statement!");

            AllVariables.FindVariable(RealToken1).Value = operand2.value;

            RealToken1 = new Token(operand1.type, operand2.value);

            return RealToken1;
        }
    }
}

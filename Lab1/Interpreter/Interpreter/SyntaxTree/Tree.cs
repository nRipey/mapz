﻿using Interpreter.SyntaxTree;
using Interpreter.SyntaxTree.Arithmetic;
using Interpreter.SyntaxTree.Html_Funcs;
using Interpreter.SyntaxTree.Logical;
using System.Collections.Generic;
using System.Windows.Forms;
using System;

namespace Interpreter
{
    class ArithmeticTree
    {
        public Tree tree;
        public List<Token> RPN;
        List<Token> tokens;
        DataFromForm data;

        public ArithmeticTree(List<Token> tokens, DataFromForm data_)
        {
            data = data_;
            this.tokens = tokens;
            
            CheckForCountOfTag();
            CheckForCountOfAttributes();
            CheckForCountOfClass();

            tree = new Tree(tokens, this);
            tree.FindRPN(this);
        }

        #region CountOfTag(<tag>) CountOfAttribute(<attribute>) CountOfClass("className") replaced by int-values
        private void CheckForCountOfTag()
        {
           new Func_CountOfTag(tokens, data);
        }
        private void CheckForCountOfAttributes()
        {
            new Func_CountOfAttribute(tokens, data);
        }        

        private void CheckForCountOfClass()
        {
          new Func_CountOfClass(tokens, data);
        }

        #endregion
        public Token FindResult()
        {
            List<AbstractOperator> operators = new List<AbstractOperator>()
            {
                new Operator_Equal(),
                new Operator_Greater(),
                new Operator_GreaterEqual(),
                new Operator_Less(),
                new Operator_LessEqual(),
                new Operator_NotEqual(),

                new Operator_Assignment(),

                new Operator_Division(),
                new Operator_Minus(),
                new Operator_Mod(),
                new Operator_Multiplication(),
                new Operator_Plus()
            };

            #region Run the arithemitc expression
            try
            {
                while (RPN.Count != 1)
                {
                    int i = 0;
                    Token temp = null;

                    for (i = 0; i < RPN.Count; ++i)
                    {
                        if (RPN[i].type == TokenTypeToLexer.OPERATOR)
                        {
                            foreach (var val in operators)
                            {
                                temp = val.GetResult(RPN[i], RPN[i - 2], RPN[i - 1], data.AllVariables);

                                if (temp != null) break;
                            }
                            RPN[i] = temp;
                            RPN.RemoveRange(i - 2, 2);
                            break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
            
            return (RPN[0].type == TokenTypeToLexer.VARIABLE) ? new Token(data.AllVariables.FindVariable(RPN[0]).Type, data.AllVariables.FindVariable(RPN[0]).Value) : RPN[0];  
        }

       public class Tree
        {
            public Tree LeftChild;
            public Tree RightChild;
            public Token CurrentValue;

            public Tree(List<Token> tokens, ArithmeticTree outer)
            {
                if (tokens.Count == 1)
                {
                    LeftChild = RightChild = null;
                    CurrentValue = tokens[0];
                    return;
                }

                #region =
                for (int i = 0; i < tokens.Count; ++i)
                {
                    if (tokens[i].value == Operator_Assignment.Value)
                    {
                        CurrentValue = tokens[i];
                        LeftChild = new Tree(tokens.GetRange(0, i), outer);
                        RightChild = new Tree(tokens.GetRange(i + 1, tokens.Count - i - 1), outer);
                        return;
                    }
                }
                #endregion

                #region == != > < >= <=
                for (int i = tokens.Count - 1; i >= 0; --i)
                {
                    if (tokens[i].value == Operator_Equal.Value || tokens[i].value == Operator_NotEqual.Value ||
                        tokens[i].value == Operator_Less.Value || tokens[i].value == Operator_Greater.Value   ||
                        tokens[i].value == Operator_LessEqual.Value || tokens[i].value == Operator_GreaterEqual.Value)
                    {
                        CurrentValue = tokens[i];
                        LeftChild = new Tree(tokens.GetRange(0, i), outer);
                        RightChild = new Tree(tokens.GetRange(i + 1, tokens.Count - i - 1), outer);
                        return;
                    }
                }
                #endregion

                #region + -
                for (int i = tokens.Count - 1; i >= 0; --i)
                {
                    if (tokens[i].value == Operator_Plus.Value || tokens[i].value == Operator_Minus.Value)
                    {
                        CurrentValue = tokens[i];
                        LeftChild = new Tree(tokens.GetRange(0, i), outer);
                        RightChild = new Tree(tokens.GetRange(i + 1, tokens.Count - i - 1), outer);
                        return;
                    }
                }
                #endregion

                #region * / %
                for (int i = tokens.Count-1; i >= 0; --i)
                {
                    if (tokens[i].value == Operator_Multiplication.Value || tokens[i].value == Operator_Division.Value || tokens[i].value == Operator_Mod.Value)
                    {
                        CurrentValue = tokens[i];
                        LeftChild = new Tree(tokens.GetRange(0, i), outer);
                        RightChild = new Tree(tokens.GetRange(i + 1, tokens.Count - i - 1), outer);
                        return;
                    }
                }
                #endregion

            }

            public void FindRPN(ArithmeticTree outer)
            {
                outer.RPN = new List<Token>();
                LRN(this, outer);
            }

            void LRN(Tree current, ArithmeticTree outer)
            {
                if (current == null) return;
                LRN(current.LeftChild, outer);
                LRN(current.RightChild, outer);
                outer.RPN.Add(current.CurrentValue);
            }
        }
    }
}

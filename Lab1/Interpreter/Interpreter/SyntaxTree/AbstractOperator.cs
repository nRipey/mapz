﻿
namespace Interpreter
{
    abstract class AbstractOperator
    {
        protected Token operand1;
        protected Token operand2;

        protected Token RealToken1;
        protected Token RealToken2;

        protected Variables AllVariables;

        public abstract Token GetResult(Token sign, Token _operand1, Token _operand2, Variables AllVariables);

        protected void GetResult(Token _operand1, Token _operand2, Variables AllVariables)
        {
            this.AllVariables = AllVariables;

            RealToken1 = _operand1;
            RealToken2 = _operand2;

            string operand1_value = (_operand1.type == TokenTypeToLexer.VARIABLE) ? AllVariables.FindVariable(_operand1).Value : _operand1.value;
            string operand2_value = (_operand2.type == TokenTypeToLexer.VARIABLE) ? AllVariables.FindVariable(_operand2).Value : _operand2.value;

            TokenTypeToLexer type1 = (_operand1.type == TokenTypeToLexer.VARIABLE) ? AllVariables.FindVariable(_operand1).Type : _operand1.type;
            TokenTypeToLexer type2 = (_operand2.type == TokenTypeToLexer.VARIABLE) ? AllVariables.FindVariable(_operand2).Type : _operand2.type;

            operand1 = new Token(type1, operand1_value);
            operand2 = new Token(type2, operand2_value);
        }
        protected bool IsOperationWithNumbers() => (operand1.type != TokenTypeToLexer.BOOL && operand1.type != TokenTypeToLexer.STRING && operand2.type != TokenTypeToLexer.BOOL && operand2.type != TokenTypeToLexer.STRING);
        
    }
}

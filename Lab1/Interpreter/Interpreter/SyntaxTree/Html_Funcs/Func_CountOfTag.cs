﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Interpreter.SyntaxTree.Html_Funcs
{
    class Func_CountOfTag
    {
        public Func_CountOfTag(List<Token> tokens, DataFromForm data)
        {
            const int CountOfTokens = 4;
            int StartIndex = 0;

            while (StartIndex < tokens.Count)
            {
                #region Find start-index of CountOfTag
                for (; StartIndex < tokens.Count; ++StartIndex)
                {
                    if (tokens[StartIndex].value == Token.Expressions[TokenType.COUNT_OF_TAG]) break;
                }
                if (tokens.Count - StartIndex < CountOfTokens || StartIndex == tokens.Count || tokens[StartIndex + 1].value != "(") return;
                #endregion

                #region Check if CountOfTag Statement is OK
                List<Token> ExpressionAssignment = ExpressionAST.GetTokensInExpressionUntilSeperator(ref StartIndex, tokens, ")");
                if (ExpressionAssignment.Count != CountOfTokens || ExpressionAssignment[ExpressionAssignment.Count - 1].value != ")") throw new Exception("Wrong CountOfTags Statement!");
                
                Token TokenTag = ExpressionAssignment[2];
                string Tag = (TokenTag.type == TokenTypeToLexer.TAG) ? TokenTag.value : throw new Exception("Wrong CountOfTags Statement!");
                #endregion

                #region Replace CountOfTag(<tag>) by int-value
                int nodes = data.html.DocumentNode.Descendants(Tag).Count();

                if (nodes == 0)
                {
                    tokens[StartIndex - 1] = new Token(TokenTypeToLexer.INT, "0");
                    tokens.RemoveRange(StartIndex - CountOfTokens, CountOfTokens - 1);
                    StartIndex -= (CountOfTokens - 1);
                }
                else
                {
                    //Change ) -> CountOfTags [Token(INT,CountOfTags)]
                    tokens[StartIndex - 1] = new Token(TokenTypeToLexer.INT, nodes.ToString());
                    tokens.RemoveRange(StartIndex - CountOfTokens, CountOfTokens - 1);
                    StartIndex -= (CountOfTokens - 1);

                }
                #endregion
            }
        }
    }
}

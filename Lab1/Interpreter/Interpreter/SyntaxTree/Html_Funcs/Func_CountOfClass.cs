﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Interpreter.SyntaxTree
{
   class Func_CountOfClass
    {
        public Func_CountOfClass(List<Token> tokens, DataFromForm data)
        {
            const int CountOfTokens = 4;
            int StartIndex = 0;

            while (StartIndex < tokens.Count)
            {
                #region Find start-index of CountOfTag
                for (; StartIndex < tokens.Count; ++StartIndex)
                {
                    if (tokens[StartIndex].value == Token.Expressions[TokenType.COUNT_OF_CLASS]) break;
                }

                if (tokens.Count - StartIndex < CountOfTokens || StartIndex == tokens.Count || tokens[StartIndex + 1].value != "(") return;
                #endregion

                #region Check if CountOfClass Statement is OK
                List<Token> ExpressionAssignment = ExpressionAST.GetTokensInExpressionUntilSeperator(ref StartIndex, tokens, ")");
                if (ExpressionAssignment.Count != CountOfTokens || ExpressionAssignment[ExpressionAssignment.Count - 1].value != ")") throw new Exception("Wrong CountOfClass Statement!");
                #endregion

                #region Replace CountOfClass("ClassName") by int-value
                Token TokenClassValue = ExpressionAssignment[2];
                string ClassValue = (TokenClassValue.type == TokenTypeToLexer.STRING) ? TokenClassValue.value
                                    : (TokenClassValue.type == TokenTypeToLexer.VARIABLE && data.AllVariables.FindVariable(TokenClassValue).Type == TokenTypeToLexer.STRING) ?
                                    data.AllVariables.FindVariable(TokenClassValue).Value : throw new Exception("Wrong FindAllByClass Statement!");

                ClassValue = "\"" + ClassValue + "\"";
                var nodes = new HtmlNodeCollection[Lexer.AllTagValues.Length];


                int Count = 0;

                for (int i = 0; i < Lexer.AllTagValues.Length; ++i)
                {
                    nodes[i] = data.html.DocumentNode.SelectNodes($"//{Lexer.AllTagValues[i]}[@class = {ClassValue}]");
                }

                foreach (var node in nodes)
                {
                    if (node != null)
                        Count += node.Count();
                }


                //Change ) -> CountOfClass [Token(INT,CountOfClass)]
                tokens[StartIndex - 1] = new Token(TokenTypeToLexer.INT, Count.ToString());
                tokens.RemoveRange(StartIndex - CountOfTokens, CountOfTokens - 1);
                StartIndex -= (CountOfTokens - 1);
                #endregion
            }
        }
    }
}

﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Interpreter.SyntaxTree.Html_Funcs
{
    class Func_CountOfAttribute
    {
        public Func_CountOfAttribute(List<Token> tokens, DataFromForm data)
        {
           const int MinCountOfTokens = 4;
           int StartIndex = 0;
  
           while (StartIndex < tokens.Count)
            {
                #region Find start-index of CountOfAttribute
                for (; StartIndex < tokens.Count; ++StartIndex)
                {  
                    if (tokens[StartIndex].value == Token.Expressions[TokenType.COUNT_OF_ATTRIBUTE]) break;
                }

                if (tokens.Count - StartIndex < MinCountOfTokens || StartIndex == tokens.Count || tokens[StartIndex + 1].value != "(") return;
                #endregion

                #region Check if CountOfAttrribute Statement is OK
                List<Token> ExpressionAssignment = ExpressionAST.GetTokensInExpressionUntilSeperator(ref StartIndex, tokens, ")");

                if (ExpressionAssignment.Count != MinCountOfTokens || ExpressionAssignment[ExpressionAssignment.Count - 1].value != ")") throw new Exception(" Wrong CountOfAttributes!");          

                Token TokenAttribute = ExpressionAssignment[2];
                string Attribute = (TokenAttribute.type == TokenTypeToLexer.ATTRIBUTE) ? TokenAttribute.value : throw new Exception("Wrong CountOfAttributes!");
                #endregion


                #region Replace CountOfAttribute(<attribute>) by int-value
                var nodes = new HtmlNodeCollection[Lexer.AllTagValues.Length];

                int Count = 0;

                for (int i = 0; i < Lexer.AllTagValues.Length; ++i)
                {
                    nodes[i] = data.html.DocumentNode.SelectNodes($"//{Lexer.AllTagValues[i]}[@{Attribute}]");
                }

                foreach (var node in nodes)
                {
                    if (node != null)
                        Count += node.Count();
                }


                //Change ) -> CountOfAttributes [Token(INT,CountOfAttributes)]
                tokens[StartIndex - 1] = new Token(TokenTypeToLexer.INT, Count.ToString());
                tokens.RemoveRange(StartIndex - MinCountOfTokens, MinCountOfTokens - 1);
                StartIndex -= (MinCountOfTokens - 1);
                #endregion
            }
        }
    }
}

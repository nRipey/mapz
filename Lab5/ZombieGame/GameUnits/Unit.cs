﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZombieGame.Zombies;

namespace ZombieGame.GameUnits
{
    public abstract class Unit
    {
        public double health { get; private set; }
        public double attack { get; private set; }

        public Unit(double health, double attack)
        {
            this.health = health;
            this.attack = attack;
        }

        virtual public double TakeDamage(double damage)
        {
            health -= damage;
            return health;
        }
    }
    public enum UnitTypes
    {
        WeakZombie,
        MediumZombie,
        StrongZombie,

        Skeleton,

        MaleSurvivor,
        FemaleSurvivor
    }

    static class PrototypeRegistry
    {
        static public Dictionary<UnitTypes, Zombie> ZombieConfigs { get; private set; } = new Dictionary<UnitTypes, Zombie>();
        static public Dictionary<UnitTypes, Survivor> SurvivorConfigs { get; private set; } = new Dictionary<UnitTypes, Survivor>();

        static public Dictionary<UnitTypes, Skeleton> SkeletonConfigs { get; private set; } = new Dictionary<UnitTypes, Skeleton>();
        static PrototypeRegistry()
        {
            ZombieConfigs.Add(UnitTypes.WeakZombie, new Zombie(6, 1, "Weak Zombie"));
            ZombieConfigs.Add(UnitTypes.MediumZombie, new Zombie(8, 2, "Medium Zombie"));
            ZombieConfigs.Add(UnitTypes.StrongZombie, new Zombie(10, 3, "Strong Zombie"));

            SkeletonConfigs.Add(UnitTypes.Skeleton, new Skeleton(4, 0.5));

            SurvivorConfigs.Add(UnitTypes.MaleSurvivor, new Survivor("MaleTemp", 10, 2));
            SurvivorConfigs.Add(UnitTypes.FemaleSurvivor, new Survivor("FemaleTemp", 9, 1.7));
        }

        public static Zombie GetZombieConfigByType(UnitTypes type)
        {
            return ZombieConfigs[type];
        }

        public static Survivor GetSurvivorConfigByType(UnitTypes type)
        {
            return SurvivorConfigs[type];
        }

        public static Skeleton GetSkeletonConfigByType(UnitTypes type)
        {
            return SkeletonConfigs[type];
        }
    }
}

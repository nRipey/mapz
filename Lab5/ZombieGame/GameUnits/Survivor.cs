﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.Ammunition;
using ZombieGame.Zombies;

namespace ZombieGame.GameUnits
{
    [Serializable()]
    public class Survivor :Unit, ICloneable, ISerializable
    {
        public string Name { get; private set; }

        private Sword sword;
        private Armor armor;
        public Survivor (string name, Survivor survivor): base (survivor.health, survivor.attack)
        {
            Name = name;
        }
        public Survivor(string name, double health, double attack) : base(health, attack)
        { 
            Name = name; 
        }

        public object Clone()
        {
            return this.MemberwiseClone() as Survivor;
        }
        
        static public Survivor CloneFromCollection(UnitTypes type)
        {
            return PrototypeRegistry.GetSurvivorConfigByType(type).MemberwiseClone() as Survivor;
        }

        /*ABSTRACT FACTORY CALL*/
        public void GetWeapon(AmmunitionFactory factory)
        {
            sword = factory.CreateSword();
            armor = factory.CreateArmor();
        }

        public string GetInfo()
        {
            string info = this.Name + Environment.NewLine;
            info += "health: " + this.health + Environment.NewLine;
            info += "attack: " + this.attack + Environment.NewLine; 

            info += "sword: ";
            if (this.sword == null)
                info += "none";
            else
                info += this.sword.Type;

            info += Environment.NewLine + "armor: ";
            if (this.armor == null)
                info += "none";
            else 
                info += this.armor.Type;
            info += Environment.NewLine;
            info += Environment.NewLine;

            return info;
        }

        public void Attack(Unit aim)
        {
            if (sword == null)
                aim.TakeDamage(this.attack);
            else
                aim.TakeDamage(sword.Hit(this.attack, aim));
        }

        public void Eat()
        {

        }

        public void GatherSupply()
        {
            
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("survivorName", Name);
            info.AddValue("health", health);
            info.AddValue("attack", attack);

            info.AddValue("sword", sword);
            info.AddValue("armor", armor);
        }

        public Survivor(SerializationInfo info, StreamingContext context):
            base((double)info.GetValue("health", typeof(double)), (double)info.GetValue("attack", typeof(double)))
        {
            Name = (string)info.GetValue("survivorName", typeof(string));
            sword = (Sword)info.GetValue("sword", typeof(Sword));
            armor = (Armor)info.GetValue("armor", typeof(Armor));
        }
    }
}

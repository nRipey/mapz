﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZombieGame;
using ZombieGame.ColonyData;

namespace ZombieGame.GameUnits
{
    [Serializable()]
    class Skeleton:Monster, ICloneable, ISerializable
    {
        public Skeleton(double health, double attack, string type = "Skeleton") : base(4, 0.5, type) { }

        public override void Attack(Colony colony)
        {
            for (int i = 0; i < colony.Survivors.Count; ++i)
                colony.Survivors[i].TakeDamage(this.attack);
        }

        static public Skeleton CloneFromCollection(UnitTypes type)
        {
            return PrototypeRegistry.GetSkeletonConfigByType(type).MemberwiseClone() as Skeleton;
        }

        public object Clone()
        {
            return this.MemberwiseClone() as Skeleton;
        }
        public Skeleton(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}

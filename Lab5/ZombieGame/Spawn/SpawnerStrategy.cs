﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.GameUnits;
using ZombieGame.Zombies;

namespace ZombieGame
{
    /*STRATEGY*/
    public class Spawner
    {
        SpawnStrategy spawnStrategy;

        public Spawner(SpawnStrategy spawnStrategy)
        {
            this.spawnStrategy = spawnStrategy;
        }

        public List<Monster> Spawn()
        {
            return spawnStrategy.Spawn();
        }

        public void ChangeStrategy(SpawnStrategy spawnStrategy)
        {
            this.spawnStrategy = spawnStrategy;
        }
    }

    public abstract class SpawnStrategy
    {
        Random random = new Random();
        protected int GetRandomNuber(int min, int max)
        {
            return random.Next(min, max);
        }

        public abstract List<Monster> Spawn();
    }

    class EasySpawnStrategy: SpawnStrategy
    {
        public override List<Monster> Spawn()
        {
            List<Monster> monsters = new List<Monster>();

            int N = GetRandomNuber(2, 5);
            for (int i = 0; i < N; ++i)
            {
                int GeneratedNumber = GetRandomNuber(1, 11);

                if(GeneratedNumber < 5)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.WeakZombie)); 
                }
                else if (GeneratedNumber < 8)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.MediumZombie));
                }
                else
                {
                    monsters.Add(Skeleton.CloneFromCollection(UnitTypes.Skeleton));
                }
            }

            for (int i = 0; i < monsters.Count; ++i)
            {
                int GeneratedNumber = GetRandomNuber(1, 25);

                if (GeneratedNumber == 1)
                    monsters[i] = new ExplosiveUnit(monsters[i]);
                else if (GeneratedNumber == 2)
                    monsters[i] = new PoisonousUnit(monsters[i]);
            }

            return monsters;
        }
    }
    class MediumSpawnStrategy : SpawnStrategy
    {
        public override List<Monster> Spawn()
        {
            List<Monster> monsters = new List<Monster>();

            int N = GetRandomNuber(3, 6);
            for (int i = 0; i < N; ++i)
            {
                int GeneratedNumber = GetRandomNuber(1, 11);

                if (GeneratedNumber < 2)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.WeakZombie));
                }
                else if (GeneratedNumber < 5)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.MediumZombie));
                }
                else if (GeneratedNumber < 9)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.StrongZombie));
                }
                else
                {
                    monsters.Add(Skeleton.CloneFromCollection(UnitTypes.Skeleton));
                }
            }

            for(int i = 0; i < monsters.Count; ++i)
            {
                int GeneratedNumber = GetRandomNuber(1, 10);

                if (GeneratedNumber == 1)
                    monsters[i] = new ExplosiveUnit(monsters[i]);
                else if(GeneratedNumber == 2)
                    monsters[i] = new PoisonousUnit(monsters[i]);
            }

            return monsters;
        }
    }
    class HardSpawnStrategy : SpawnStrategy
    {
        public override List<Monster> Spawn()
        {
            List<Monster> monsters = new List<Monster>();

            int N = GetRandomNuber(5, 8);
            for (int i = 0; i < N; ++i)
            {
                int GeneratedNumber = GetRandomNuber(1, 11);

                if (GeneratedNumber < 3)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.MediumZombie));
                }
                else if (GeneratedNumber < 8)
                {
                    monsters.Add(Zombie.CloneFromCollection(UnitTypes.StrongZombie));
                }
                else
                {
                    monsters.Add(Skeleton.CloneFromCollection(UnitTypes.Skeleton));
                }
            }

            for (int i = 0; i < monsters.Count; ++i)
            {
                int GeneratedNumber = GetRandomNuber(1, 13);

                if(GeneratedNumber == 1)
                {
                    monsters[i] = new ExplosiveUnit(monsters[i]);
                    monsters[i] = new PoisonousUnit(monsters[i]);
                }
                else if (GeneratedNumber == 2 || GeneratedNumber == 3)
                    monsters[i] = new ExplosiveUnit(monsters[i]);
                else if (GeneratedNumber == 4 || GeneratedNumber == 5)
                    monsters[i] = new PoisonousUnit(monsters[i]);
            }

            return monsters;
        }
    }
}

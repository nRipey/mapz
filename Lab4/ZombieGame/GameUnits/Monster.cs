﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.ColonyData;

namespace ZombieGame.GameUnits
{
    [Serializable()]
    public abstract class Monster:Unit, ISerializable
    {
        public string Type { get; private set; }
        public Monster(double health, double attack, string type) : base(health, attack) {
            this.Type = type;
        }

        abstract public void Attack(Colony colony);

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("type", Type);
            info.AddValue("health", health);
            info.AddValue("attack", attack);
        }

        public Monster(SerializationInfo info, StreamingContext context) :
    base((double)info.GetValue("health", typeof(double)), (double)info.GetValue("attack", typeof(double)))
        {
            Type = (string)info.GetValue("type", typeof(string));
        }
    }
}

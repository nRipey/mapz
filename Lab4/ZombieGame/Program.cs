﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZombieGame.MenuHandler;

namespace ZombieGame
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            actionInvoker = new ButtonActionInvoker();
            commandReceiver = new MenuCommandReceiver();

            Application.Run(new StartingMenu());

        }
        static public ButtonActionInvoker actionInvoker { get; private set; }
        static public MenuCommandReceiver commandReceiver { get; private set; }
    }
}

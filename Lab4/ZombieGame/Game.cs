﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZombieGame.ColonyData;
using ZombieGame.GameUnits;
using ZombieGame.Resources;
using ZombieGame.FieldRender;

namespace ZombieGame
{
    public partial class GameForm : Form
    {
        public static int Seconds { get; private set; }
        public static int daysAlive { get; private set; }
        public static Colony myColony { get; private set; }
        public FieldState fieldState { get; set; }
        public Spawner spawner { get; private set; }
        public static List<Monster> monsters { get; private set; }
        public GameForm()
        {
            InitializeComponent();
            this.CenterToScreen();

            myColony = Colony.Reset(NewColonyMenu.ColonyName);
            spawner = new Spawner(new HardSpawnStrategy());
            fieldState = new DayState();
            monsters = new List<Monster>();
            Seconds = 10;
            daysAlive = 1;

            DoInitActions();
            this.Render();
        }

        public GameForm(GameMemento memento)
        {
            InitializeComponent();
            this.CenterToScreen();

            Seconds = memento.Time;
            
            if (Seconds > 0)
                fieldState = new DayState();
            else
                fieldState = new NightState();

            daysAlive = memento.DaysAlive;
            if (daysAlive < 5)
                spawner = new Spawner(new EasySpawnStrategy());
            else if (daysAlive < 10)
                spawner = new Spawner(new MediumSpawnStrategy());
            else
                spawner = new Spawner(new HardSpawnStrategy());

            myColony = Colony.GetInstance(memento.colony);
            monsters = memento.monsters;
            
            DoInitActions();
            this.Render();
        }

        private void DoInitActions()
        {
            WelcomeBox.Text = $"Welcome to {myColony.Name}";
            SurvivorInfoBox.Text = myColony.GetSurvivalInfo();

            FoodBox.Text = myColony.GetResourceInfo(ResourceType.Food).ToString();
            WoodBox.Text = myColony.GetResourceInfo(ResourceType.Wood).ToString();
            StoneBox.Text = myColony.GetResourceInfo(ResourceType.Stone).ToString();
            WalltextBox.Text = myColony.WallState.ToString();
            DaystextBox.Text = daysAlive.ToString();

            SurviorLayoutPanel.Controls.Clear();
            foreach (var survivor in myColony.Survivors)
            {
                AddButton(SurviorLayoutPanel, survivor);
            }
        }

        private void AddButton(FlowLayoutPanel panel, Survivor survivor)
        {
            Button button = new Button();

            button.Width = 100;
            button.Height = 70;
            button.ForeColor = Color.Black;
            button.Font = new Font(button.Font.FontFamily, 12);
            button.Text = survivor.Name;
            button.Tag = survivor;

            ToolTip healthToolTip = new ToolTip();
            healthToolTip.SetToolTip(button, survivor.health.ToString());
            panel.Controls.Add(button);
        }

        private void MenuButton_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.ShowDialog();   
        }

        private void foodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO
            var clickedButton = SurvivorActionContexMenu.SourceControl;

            LogtextBox.Text += ((Survivor)clickedButton.Tag).Name + " is gathering food";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Seconds == 0)
            {
                timer1.Stop();

                fieldState = new NightState();

                if( (monsters == null) || !(monsters.Any()))
                {
                    monsters = spawner.Spawn();

                    Render();
                }
            }
            else
            {
                Seconds--;
                TimetextBox.Text = Seconds.ToString();
            }
        }

        private void Render()
        {
            fieldState.Render(this);
        }

        private void TestButton_Click(object sender, EventArgs e)
        {

        }

        static public void TakeTurn(object sender, EventArgs e, Survivor survivor)
        {

        }

        private void attackRandomlyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Random random = new Random();

            int monsterAimIndex = random.Next(0, MonsterLayoutPanel.Controls.Count);

            Button SurvivorButton = (Button)NightSurvivorMenuStrip.SourceControl;
            
            ((Survivor)SurvivorButton.Tag).Attack((Monster)MonsterLayoutPanel.Controls[monsterAimIndex].Tag);
            LogtextBox.Text += $"{((Survivor)((Button)SurvivorButton).Tag).Name} " + "attacked "
                + $"{((Monster)MonsterLayoutPanel.Controls[monsterAimIndex].Tag).Type} ({monsterAimIndex})\r\n";

            int SurvivorAimIndex = random.Next(0, SurviorLayoutPanel.Controls.Count);
            ((Monster)MonsterLayoutPanel.Controls[monsterAimIndex].Tag).Attack(myColony);
        }
    }
}

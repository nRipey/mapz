﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZombieGame.GameUnits;
using ZombieGame.Resources;
using ZombieGame.Ammunition;
using ZombieGame.FieldRender;
using System.Runtime.Serialization;

namespace ZombieGame.ColonyData
{
    /*SINGLETON*/
    /*FACADE*/
    [Serializable()]
    public class Colony:ISerializable
    {
        private static Colony instance;
        public Resource ColonyResouces { get; private set; }
        public List<Survivor> Survivors { get; private set; }
        public string Name { get; private set; }
        public int WallState { get; private set; }

        private static object syncRoot = new Object();

        private Colony(string name)
        {
            Name = name;

            WallState = 20;
            ColonyResouces = new Resource();
            Survivors = new List<Survivor>();

            Survivors.Add(new Survivor("John", Survivor.CloneFromCollection(UnitTypes.MaleSurvivor)));
            Survivors.Add(new Survivor("Jesse", Survivor.CloneFromCollection(UnitTypes.MaleSurvivor)));
            Survivors.Add(new Survivor("Walt", Survivor.CloneFromCollection(UnitTypes.MaleSurvivor)));

            Survivors.Add(new Survivor("Julia", Survivor.CloneFromCollection(UnitTypes.FemaleSurvivor)));
            Survivors.Add(new Survivor("Billie", Survivor.CloneFromCollection(UnitTypes.FemaleSurvivor)));
        }

        private Colony(Colony colony)
        {
            Name = colony.Name;
            WallState = colony.WallState;
            ColonyResouces = new Resource(colony.ColonyResouces.Storage[ResourceType.Food], 
                colony.ColonyResouces.Storage[ResourceType.Wood], colony.ColonyResouces.Storage[ResourceType.Stone]);
            Survivors = new List<Survivor>(colony.Survivors);
        }

        public static Colony GetInstance(string name)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                        instance = instance?? new Colony(name);
                }
            }
            return instance;
        }

        public static Colony GetInstance(Colony colony)
        {
            instance = new Colony(colony);

            return instance;
        }

        public static Colony Reset(string name)
        {
            instance = new Colony(name);
            return instance;
        }

        public void ProvideResource(Survivor survivor)
        {
            
        }

        public void DamageWall(int damage)
        {
            WallState -= damage;
        }

        public string GetSurvivalInfo()
        {
            string info = "Total: " + Survivors.Count + Environment.NewLine + Environment.NewLine;

            foreach (Survivor survivor in Survivors)
                info += survivor.GetInfo();

            return info;
        }
        public int GetResourceInfo(ResourceType type)
        {
            return this.ColonyResouces.Storage[type];
        }

        public void SetWeaponToSurvivor(Survivor survivor, AmmunitionFactory factory)
        {
            //TODO
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Survivors", instance.Survivors);
            info.AddValue("WallState", WallState);

            info.AddValue("Food", ColonyResouces.Storage[ResourceType.Food]);
            info.AddValue("Wood", ColonyResouces.Storage[ResourceType.Wood]);
            info.AddValue("Stone", ColonyResouces.Storage[ResourceType.Stone]);
        }

        public Colony(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue("Name", typeof(string));
            WallState = (int)info.GetValue("WallState", typeof(int));

            Survivors = (List<Survivor>)info.GetValue("Survivors", typeof(List<Survivor>));

            ColonyResouces = new Resource();
            ColonyResouces.Storage[ResourceType.Food] = (int)info.GetValue("Food", typeof(int));
            ColonyResouces.Storage[ResourceType.Wood] = (int)info.GetValue("Wood", typeof(int));
            ColonyResouces.Storage[ResourceType.Stone] = (int)info.GetValue("Stone", typeof(int));
        }
    }
}

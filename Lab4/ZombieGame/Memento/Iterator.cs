﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieGame;

namespace ZombieGame.IteratorInfo
{
    public interface Iterator
    {
        object First();
        object Last();
        object Previous();
        object Next();
        object Current();
        bool IsDone();
    }

    interface Iteratable
    {
        int Count { get; }
        Iterator CreateIterator();
        GameMemento this[int index] { get; }
    }

    public class MementoIterator : Iterator
    {
        private readonly GameHistory gameHistory;
        private int current;

        public MementoIterator(GameHistory gameHistory)
        {
            this.gameHistory = gameHistory;
        }

        public object Current()
        {
            return gameHistory[current];
        }

        public object First()
        {
            return gameHistory[0];
        }

        public bool IsDone()
        {
            return current >= gameHistory.Count;
        }

        public object Last()
        {
            return gameHistory[gameHistory.Count - 1];
        }

        public object Next()
        {
            object rez = null;

            if (++current < gameHistory.Count)
                rez = gameHistory[current];

            return rez;
        }

        public object Previous()
        {
            object rez = null;

            if (--current > 0)
                rez = gameHistory[current];
            return rez;
        }
    }
}

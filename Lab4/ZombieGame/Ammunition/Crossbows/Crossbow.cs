﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.GameUnits;

namespace ZombieGame.Ammunition.Crossbows
{
    abstract class Crossbow
    {
        public void Attack(Unit aim)
        {
            Charge();
            DoSpecialEffect();
            Shoot(aim);
        }

        abstract protected void Charge();
        virtual protected void DoSpecialEffect() { }
        abstract protected void Shoot(Unit aim);
    }

    class SimpleCrossbow : Crossbow
    {
        protected override void Charge()
        {
            throw new NotImplementedException();
            //Some logic of making arrows
        }

        protected override void Shoot(Unit aim)
        {
            aim.TakeDamage(2);
        }
    }
    class MagicCrossbow : Crossbow
    {
        protected override void Charge()
        {
            throw new NotImplementedException();
            //Some logic of making arrows
        }

        protected override void DoSpecialEffect()
        {
            //Some magic effect logic
        }

        protected override void Shoot(Unit aim)
        {
            aim.TakeDamage(4);
        }
    }
}

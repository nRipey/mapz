﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.ColonyData;

namespace ZombieGame.GameUnits
{
    [Serializable()]
    abstract class MonsterDecorator:Monster, ISerializable
    {
        protected Monster monster;
        Random random = new Random();

        public MonsterDecorator(double health, double attack, Monster monster, string decoration) : base(health, attack, decoration)
        {
            this.monster = monster;
        }
        public MonsterDecorator(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
        public int GenerateNumber(int min, int max)
        {
            return random.Next(min, max);
        }
    }

    [Serializable()]
   class ExplosiveUnit: MonsterDecorator, ISerializable
   {
        public ExplosiveUnit(Monster monster) : base(monster.health, monster.attack, monster, monster.Type + " explosive") { }
        public ExplosiveUnit(SerializationInfo info, StreamingContext context) : base(info, context)
        { }

        public override void Attack(Colony colony)
        {
            monster.Attack(colony);

            if(GenerateNumber(0, 101) < 50)
                colony.DamageWall(GenerateNumber(2, 5));        
        }
   }

    [Serializable()]
    class PoisonousUnit: MonsterDecorator
    {
        public PoisonousUnit(Monster monster) : base(monster.health, monster.attack, monster, monster.Type + " poisonous") { }

        public override void Attack(Colony colony)
        {
            monster.Attack(colony);

            /*
             * Damage Using State
            colony.Survivors[GenerateNumber(0, colony.Survivors.Count)]
            */
        }

        public PoisonousUnit(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}

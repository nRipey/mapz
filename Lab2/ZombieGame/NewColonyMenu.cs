﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZombieGame
{
    public partial class NewColonyMenu : Form
    {
        public static string ColonyName;
        public NewColonyMenu()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            if(ColonyNametextBox.Text.Length == 0)
            {
                MessageBox.Show("Name your colony!");
                return;
            }

            this.Hide();


            ColonyName = ColonyNametextBox.Text;
            GameForm game = new GameForm();
            game.Closed += (s, args) => this.Close();

            game.Show();
        }
    }
}

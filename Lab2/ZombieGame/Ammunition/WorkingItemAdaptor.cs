﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieGame.GameUnits;

namespace ZombieGame.Ammunition
{
    class WorkingItemAdaptor: Sword
    {
        WorkingItem item;

        public WorkingItemAdaptor(WorkingItem item)
        {
            this.item = item;
        }

        public override double Hit(double attack, Unit unit)
        {
            item.ReduceTimes();    

            return attack * 0.2;
        }
    }
}

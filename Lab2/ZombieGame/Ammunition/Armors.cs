﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieGame.Ammunition
{
    public abstract class Armor
    {
        public string Type { get; private set; }

        protected static readonly double defence = 0.15;
        public Armor(string type)
        {
            this.Type = type;
        }
        public abstract void Defend();
    }

    class SimpleArmor : Armor
    {
        public SimpleArmor() : base("Simple") { }
        public override void Defend()
        {
            throw new NotImplementedException();
        }
    }

    class StrongArmor: Armor
    {
        public StrongArmor() : base("Strong") { }
        public override void Defend()
        {
            throw new NotImplementedException();
        }
    }

    class MagicArmor : Armor
    {
        public MagicArmor() : base("Magic") { }
        public override void Defend()
        {
            throw new NotImplementedException();
        }
    }
}

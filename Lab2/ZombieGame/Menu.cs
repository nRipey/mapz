﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZombieGame.MenuHandler;
 

namespace ZombieGame
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ResumeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewColonyButton_Click(object sender, EventArgs e)
        {
            Program.actionInvoker.SetCommand(new NewColonyCommand(Program.commandReceiver));

            Program.actionInvoker.DoAction();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Program.actionInvoker.SetCommand(new SaveCommand(Program.commandReceiver));

            Program.actionInvoker.DoAction();
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            Program.actionInvoker.SetCommand(new LoadCommand(Program.commandReceiver));

            Program.actionInvoker.DoAction();
        }
    }
}

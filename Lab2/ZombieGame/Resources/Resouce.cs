﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZombieGame.Resources
{
    public enum ResourceType
    {
        Food,
        Wood,
        Stone
    }
    public class Resource
    {
        public Dictionary<ResourceType, int> Storage;

        public Resource()
        {
            Storage = new Dictionary<ResourceType, int>();

            Storage.Add(ResourceType.Food, 30);
            Storage.Add(ResourceType.Wood, 20);
            Storage.Add(ResourceType.Stone, 10);
        }

        public Resource(int food, int wood, int stone)
        {
            Storage = new Dictionary<ResourceType, int>();

            Storage.Add(ResourceType.Food, food);
            Storage.Add(ResourceType.Wood, wood);
            Storage.Add(ResourceType.Stone, stone);
        }

        public void ConsumeResource(ResourceType type, int amount)
        {
            Storage[type] -= amount;
        }

        public void FillResource(ResourceType type, int amount)
        {
            Storage[type] += amount;
        }
    }
}
